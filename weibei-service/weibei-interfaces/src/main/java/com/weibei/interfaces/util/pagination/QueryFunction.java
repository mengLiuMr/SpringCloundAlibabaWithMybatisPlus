package com.weibei.interfaces.util.pagination;

import java.util.List;

/**
 * @author daixilong
 * @version 1.0.0
 * @ClassName QueryFunction.java
 * @Description TODO
 * @Date 2021/5/1 9:52
 */
@FunctionalInterface
public interface QueryFunction<Vo,Dto> {
    List<Vo> listByCondition(Dto dto);
}
