package com.weibei.interfaces.service.system.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.weibei.db.domain.system.CitroenCarNamingModel;
import com.weibei.db.mapper.system.CitroenCarNamingMapper;
import com.weibei.interfaces.service.system.CitroenCarNamingService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liumeng
 * @since 2021-08-31
 */
@Service
public class CitroenCarNamingServiceImpl extends ServiceImpl<CitroenCarNamingMapper, CitroenCarNamingModel> implements CitroenCarNamingService {

    @Override
    public List<CitroenCarNamingModel> read() {
        LambdaQueryWrapper<CitroenCarNamingModel> queryWrapper = Wrappers.<CitroenCarNamingModel>lambdaQuery();
        queryWrapper.like(CitroenCarNamingModel::getOpenid,"oP3");
        return this.list(queryWrapper);
    }

    @Override
    public List<CitroenCarNamingModel> sqlRead() {
        return this.baseMapper.sqlRead();
    }
}
