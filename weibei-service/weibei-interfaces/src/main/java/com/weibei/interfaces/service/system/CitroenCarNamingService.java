package com.weibei.interfaces.service.system;

import com.baomidou.mybatisplus.extension.service.IService;
import com.weibei.db.domain.system.CitroenCarNamingModel;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liumeng
 * @since 2021-08-31
 */
public interface CitroenCarNamingService extends IService<CitroenCarNamingModel> {

    List<CitroenCarNamingModel> read();

    List<CitroenCarNamingModel> sqlRead();
}
