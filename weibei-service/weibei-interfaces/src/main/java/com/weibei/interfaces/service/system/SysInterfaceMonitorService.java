package com.weibei.interfaces.service.system;

import com.baomidou.mybatisplus.extension.service.IService;
import com.weibei.common.core.domain.Result;
import com.weibei.db.domain.web.TbInterfaceMonitor;
import com.weibei.db.dto.system.input.InterfaceMonitorPageInput;
import com.weibei.system.domain.SysUser;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface SysInterfaceMonitorService extends IService<TbInterfaceMonitor> {

    Result getPage(SysUser sysUser, InterfaceMonitorPageInput input) throws Exception;

}
