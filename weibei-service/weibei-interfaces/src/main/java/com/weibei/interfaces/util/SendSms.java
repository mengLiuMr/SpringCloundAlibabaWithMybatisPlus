package com.weibei.interfaces.util;

import com.weibei.common.utils.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;

@Component
@Slf4j
public class SendSms {

    @Value("${sms.url}")
    private String url;
    @Value("${sms.ecid}")
    private String ecid;
    @Value("${sms.userCode}")
    private String userCode;
    @Value("${sms.userPass}")
    private String userPass;

//    private templates final String url = "http://58.48.109.7:12007/SmsApi.asmx";
//    private templates final String ecid = "hb00081";
//    private templates final String userCode = "dfcyc-yzm";
//    private templates final String userPass = "123456";

    public void sendSms(String phoneStr, String msg) {
        try {
            //创建url地址
            URL urlConn = new URL(url);
            //打开连接
            URLConnection conn = urlConn.openConnection();
            //转换成HttpURL
            HttpURLConnection httpConn = (HttpURLConnection) conn;
            //打开输入输出的开关
            httpConn.setDoInput(true);
            httpConn.setDoOutput(true);
            //设置请求方式
            httpConn.setRequestMethod("POST");
            //设置请求的头信息
            httpConn.setRequestProperty("Content-type", "text/xml;charset=UTF-8");
            //拼接请求消息
//            msg = new String(msg.getBytes(),"UTF-8");
            String data = "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                        + "<soap:Body>"
                        + "<SendSms xmlns=\"http://tempuri.org/\">"
                        + "<ECID>" + ecid + "</ECID>"
                        + "<UserCode>" + userCode + "</UserCode>"
                        + "<UserPass>" + userPass + "</UserPass>"
                        + "<PhoneStr>" + phoneStr + "</PhoneStr>"
                        + "<Msg>" + msg + "</Msg>"
                        + "<SendTime>" + DateUtil.formatDateTime(new Date(), "yyyy-MM-dd HH-mm-ss") + "</SendTime>"
                        + "</SendSms>"
                        + "</soap:Body>"
                        + "</soap:Envelope>";
            //获得输出流
            OutputStream out = httpConn.getOutputStream();
            //发送数据
            out.write(data.getBytes());
            //判断请求成功
            if (httpConn.getResponseCode() == 200) {
                //获得输入流
                InputStream in = httpConn.getInputStream();
                //使用输入流的缓冲区
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                StringBuffer sb = new StringBuffer();
                String line = null;
                //读取输入流
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
//                //创建sax的读取器
//                SAXReader saxReader = new SAXReader();
//                //创建文档对象
//                Document doc = saxReader.read(new StringReader(sb.toString()));
//                //获得请求响应return元素
//                List<Element> eles = doc.selectNodes("SendSmsResult");
//                for (Element ele : eles) {
//                    System.out.println(ele.getText());
//                }
            }
            log.info("###发送短信返回：" + httpConn.getResponseCode() + "," + httpConn.getResponseMessage());
        } catch (Exception e) {
            e.printStackTrace();
            log.error("发送短信异常",e);
        }
    }

}
