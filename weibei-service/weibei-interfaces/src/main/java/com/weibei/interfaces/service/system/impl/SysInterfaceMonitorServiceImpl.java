package com.weibei.interfaces.service.system.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.weibei.common.core.domain.Constant;
import com.weibei.common.core.domain.Result;
import com.weibei.common.core.domain.RetCodeEnum;
import com.weibei.common.utils.response.ResponseUtils;
import com.weibei.common.utils.security.Md5Utils;
import com.weibei.db.domain.web.TbInterfaceMonitor;
import com.weibei.db.dto.system.input.InterfaceMonitorPageInput;
import com.weibei.db.dto.system.output.InterfaceMonitorPageOutput;
import com.weibei.db.mapper.system.SysInterfaceMonitorMapper;
import com.weibei.interfaces.service.system.SysInterfaceMonitorService;
import com.weibei.system.domain.SysUser;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @program: plat_cloud
 * @description: 接口监控 service实现
 * @date: 2020-04-22 11:09
 **/
@Slf4j
@Service
public class SysInterfaceMonitorServiceImpl  extends ServiceImpl<SysInterfaceMonitorMapper, TbInterfaceMonitor> implements SysInterfaceMonitorService {

    @Autowired
    private SysInterfaceMonitorMapper sysInterfaceMonitorMapper;

    @Override
    public Result getPage(SysUser sysUser, InterfaceMonitorPageInput input) throws Exception{
        if (input.getPageNum() == null || input.getPageSize() == null){
            return ResponseUtils.fail(RetCodeEnum.CODE_401.getCode(),"分页参数不能为空");
        }
        if (StringUtils.isBlank(input.getSign())) {
            return ResponseUtils.fail(RetCodeEnum.CODE_401.getCode(), "签名不能为空");
        }
        //验证签名
        String mysign = Md5Utils.hash(Constant.PARAM_SECURITY_SIGN_STR + sysUser.getLoginName());
        if (!input.getSign().equals(mysign)){
            return ResponseUtils.fail(RetCodeEnum.CODE_401.getCode(),"非法请求");
        }
        PageHelper.startPage(input.getPageNum(), input.getPageSize());
        List<InterfaceMonitorPageOutput> outputs = sysInterfaceMonitorMapper.getList(input);
        PageInfo<InterfaceMonitorPageOutput> data = new PageInfo<>(outputs);
        return ResponseUtils.ok(data);
    }

}
