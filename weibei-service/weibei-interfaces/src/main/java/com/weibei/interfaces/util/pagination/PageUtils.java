package com.weibei.interfaces.util.pagination;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.weibei.common.core.domain.PageInfoResult;
import com.weibei.db.domain.PageBean;

import java.util.List;

/**
 * @author daixilong
 * @version 1.0.0
 * @ClassName PageUtils.java
 * @Description TODO
 * @Date 2021/4/30 17:36
 */
public class PageUtils {
    /**
     *功能描述 单独分页不依赖service
     * @author daixilong
     * @date 2021/5/1
     * @param queryFunction
     * @param q
     * @return com.weibei.common.core.domain.PageInfoResult
     */
    public static  <T,Q extends PageBean> PageInfoResult toPage(QueryFunction<T,Q> queryFunction, Q q)
    {
        PageHelper.startPage(q.getPageNum(),q.getPageSize());
        List<T> list = queryFunction.listByCondition(q);
        PageInfo<T> pageInfo = new PageInfo(list);
        PageInfoResult<T> pageInfoResult = new PageInfoResult<>();
        pageInfoResult.setRows(list);
        pageInfoResult.setPageNum(pageInfo.getPageNum());
        pageInfoResult.setPageSize(pageInfo.getPageSize());
        pageInfoResult.setTotal(pageInfo.getTotal());
        return pageInfoResult;
    }
}
