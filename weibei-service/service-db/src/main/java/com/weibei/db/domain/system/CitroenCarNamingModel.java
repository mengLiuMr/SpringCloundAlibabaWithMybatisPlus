package com.weibei.db.domain.system;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author liumeng
 * @since 2021-08-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("citroen_car_naming")
@ApiModel(value="CitroenCarNamingModel对象", description="")
public class CitroenCarNamingModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "用户openid")
    @TableField("openid")
    private String openid;

    @ApiModelProperty(value = "级别1名称")
    @TableField("level_one")
    private String levelOne;

    @ApiModelProperty(value = "级别2名称")
    @TableField("level_two")
    private String levelTwo;

    @ApiModelProperty(value = "级别3名称")
    @TableField("level_three")
    private String levelThree;

    @ApiModelProperty(value = "级别4名称")
    @TableField("level_four")
    private String levelFour;

    @ApiModelProperty(value = "命名理由")
    @TableField("reason")
    private String reason;

    @ApiModelProperty(value = "提交时间")
    @TableField("submit_time")
    private LocalDateTime submitTime;


    public static final String ID = "id";

    public static final String OPENID = "openid";

    public static final String LEVEL_ONE = "level_one";

    public static final String LEVEL_TWO = "level_two";

    public static final String LEVEL_THREE = "level_three";

    public static final String LEVEL_FOUR = "level_four";

    public static final String REASON = "reason";

    public static final String SUBMIT_TIME = "submit_time";

}
