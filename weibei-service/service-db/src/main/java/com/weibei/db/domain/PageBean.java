package com.weibei.db.domain;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Objects;


public class PageBean implements Serializable {
    @ApiModelProperty(value = "当前页数(默认1)",example = "1")
    private Integer pageNum;
    @ApiModelProperty(value = "每页条数(默认10)",example = "10")
    private Integer pageSize;

    public Integer getPageNum() {
        return Objects.isNull(pageNum)? 1:pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getPageSize() {
        return Objects.isNull(pageSize)?10:pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
}
