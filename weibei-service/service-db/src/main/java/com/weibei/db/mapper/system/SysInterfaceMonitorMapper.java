package com.weibei.db.mapper.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.weibei.db.domain.web.TbInterfaceMonitor;
import com.weibei.db.dto.system.input.InterfaceMonitorPageInput;
import com.weibei.db.dto.system.output.InterfaceMonitorPageOutput;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 接口监控 数据层
 *
 * @author weibei
 */
@Mapper
public interface SysInterfaceMonitorMapper extends BaseMapper<TbInterfaceMonitor> {

    List<InterfaceMonitorPageOutput> getList(InterfaceMonitorPageInput input) throws Exception;

}
