package com.weibei.db.domain.web;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 接口监控 tb_inteface_monitor
 *
 * @author weibei
 * @date 2020-08-14
 */
@Data
public class TbInterfaceMonitor{
    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /** ip地址 */
    private String ipAddr;

    /** 接口请求地址 */
    private String reqUrl;

    /** 接口请求参数 */
    private String reqParm;

    /** 接口返回 */
    private String respMsg;

    /** 接口返回码 */
    private String respCode;

    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "新增时间",example = "2020-04-27 21:27:44")
    private Date addTime;

    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新时间",example = "2020-04-27 21:27:44")
    private Date updateTime;

    @ApiModelProperty(value = "是否删除",example = "0：否，1：是")
    private Integer deleted;

}
