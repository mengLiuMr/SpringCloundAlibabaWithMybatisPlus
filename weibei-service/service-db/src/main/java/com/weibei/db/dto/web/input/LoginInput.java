package com.weibei.db.dto.web.input;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("小程序微信登录注册入参")
@Data
public class LoginInput {

    @ApiModelProperty(value = "临时登录凭证code",required = true, example = "")
    private String code;

    @ApiModelProperty(value = "手机号", example = "134123456778")
    private String mobile;


    @ApiModelProperty(value = "用户信息",required = true, example = "")
    private UserSourceInput userInfo;


    @ApiModelProperty(value = "微信授权获取用户信息时，返回的加密数据，明文",required = true, example = "")
    private String encryptedData;

    @ApiModelProperty(value = "加密算法的初始向量",required = true, example = "")
    private String iv;

    @ApiModelProperty(value = "推荐人邀请码", example = "")
    private String invitationCodeUser;

    @ApiModelProperty(value = "经销商邀请码", example = "")
    private String invitationCodeAgency;

    @ApiModelProperty(value = "活动邀请码", example = "")
    private String invitationCodeActivity;

}
