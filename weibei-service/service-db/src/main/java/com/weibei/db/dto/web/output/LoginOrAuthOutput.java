package com.weibei.db.dto.web.output;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @description: 登录或者注册认证输出对象
 **/
@ApiModel("登录或者注册返回")
@Data
public class LoginOrAuthOutput {

    @ApiModelProperty(value = "用户id",example = "")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    @ApiModelProperty(value = "账号",example = "")
    private String username;

    @ApiModelProperty(value = "昵称",example = "")
    private String nickname;

    @ApiModelProperty(value = "手机",example = "")
    private String mobile;

    @ApiModelProperty(value = "头像",example = "")
    private String avatar;


    @ApiModelProperty(value = "最后登录时间",example = "")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date lastLoginTime;

    @ApiModelProperty(value = "token",example = "")
    private String token;

    @ApiModelProperty(value = "token过期时间",example = "")
    private Date tokenExpire;

    @ApiModelProperty(value = "我的邀请码",example = "")
    private String invitationCodeMy;

    @ApiModelProperty(value = "用户邀请人码",example = "")
    private String invitationCodeOther;

    @ApiModelProperty(value = "经销商邀请码",example = "")
    private String invitationCodeAgency;

    @ApiModelProperty(value = "活动邀请码",example = "")
    private String invitationCodeActivity;

    @ApiModelProperty(value = "是否为新注册的用户，0-登录 1-注册",example = "1")
    private Integer isRegist;

    @ApiModelProperty(value = "昵称长度",example = "")
    private Integer nameWordCount;

}
