package com.weibei.db.dto.web.input;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @description: 手机登录注册输入对象
 **/
@ApiModel("小程序手机登录或者注册入参")
@Data
public class LoginAuthViaMobileInput {

    @ApiModelProperty(value = "临时登录凭证code",required = true, example = "")
    private String code;

    /**
     * 短信类型
     */
    @ApiModelProperty(value = "短信类型",required = true,example = "LOGIN")
    private String msgType;

    /**
     * 手机号
     */
    @ApiModelProperty(value = "手机号",required = true,example = "15312345678")
    private String mobile;

    /**
     * 验证码
     */
    @ApiModelProperty(value = "验证码",required = true,example = "648230")
    private String msgCode;

    /**
     * 邀请码
     */
    @ApiModelProperty(value = "用户邀请码",example = "")
    private String invitationCodeOther;

    @ApiModelProperty(value = "邀请人",example = "")
    private String invitationCodeUser;

    @ApiModelProperty(value = "经销商邀请码",example = "HUB44")
    private String invitationCodeAgency;

    @ApiModelProperty(value = "活动邀请码",example = "")
    private String invitationCodeActivity;

}
