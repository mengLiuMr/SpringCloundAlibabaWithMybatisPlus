package com.weibei.db.dto.web.output;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @description: 获取微信openId出参
 **/
@ApiModel("获取微信openId出参")
@Data
public class GetOpenIdOutput {

    @ApiModelProperty(value = "微信openId",example = "")
    private String openId;

}
