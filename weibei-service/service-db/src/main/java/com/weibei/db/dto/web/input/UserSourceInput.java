package com.weibei.db.dto.web.input;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @program: plat_cloud
 * @description: 用户来源信息对象
 * @author: leizhiming
 * @date: 2020-05-08 15:27
 **/
@ApiModel("用户来源信息对象入参")
@Data
public class UserSourceInput {

    @ApiModelProperty(value = "用户id",example = "后台自动获取，无需传值")
    private Long userId;

    @ApiModelProperty(value = "昵称",example = "")
    private String nickName;

    @ApiModelProperty(value = "头像url",example = "")
    private String avatarUrl;

    @ApiModelProperty(value = "邀请码",example = "")
    private String invitationCodeOther;
}
