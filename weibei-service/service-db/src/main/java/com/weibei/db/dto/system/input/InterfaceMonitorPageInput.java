package com.weibei.db.dto.system.input;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 接口监控分页查询入参
 */
@Data
@ApiModel(value = "接口监控分页查询入参")
public class InterfaceMonitorPageInput {

    @ApiModelProperty(value = "ip地址",example = "192.168.1.1")
    private String ipAddr;

    @ApiModelProperty(value = "接口请求地址",example = "/web/xxx/add")
    private String reqUrl;

    @ApiModelProperty(value = "接口请求参数",example = "xxx")
    private String reqParm;

    @ApiModelProperty(value = "接口返回",example = "xxx")
    private String respMsg;

    @ApiModelProperty(value = "接口返回码",example = "200")
    private String respCode;

    @ApiModelProperty(value = "页数",required = true,example = "1")
    private Integer pageNum;

    @ApiModelProperty(value = "分页大小",required = true,example = "10")
    private Integer pageSize;

    @ApiModelProperty(value = "签名",required = true,example = "xxx")
    private String sign;

}
