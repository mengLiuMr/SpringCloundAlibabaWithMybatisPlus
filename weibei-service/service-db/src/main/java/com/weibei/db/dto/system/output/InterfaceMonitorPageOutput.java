package com.weibei.db.dto.system.output;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 接口监控分页查询出参
 *
 * @author weibei
 * @date 2020-07-15
 */
@Data
@ApiModel("接口监控分页查询出参")
public class InterfaceMonitorPageOutput {

    @ApiModelProperty(value = "id",example = "1")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    @ApiModelProperty(value = "ip地址",example = "192.168.1.1")
    private String ipAddr;
    @ApiModelProperty(value = "接口请求地址",example = "/web/xxx/add")
    private String reqUrl;
    @ApiModelProperty(value = "接口请求参数",example = "xxx")
    private String reqParm;
    @ApiModelProperty(value = "接口返回",example = "xxx")
    private String respMsg;
    @ApiModelProperty(value = "接口返回码",example = "200")
    private String respCode;
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "新增时间", example = "2020-09-01 11:00:11")
    private Date addTime;

}
