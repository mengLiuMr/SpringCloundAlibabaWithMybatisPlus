package com.weibei.db.dto.web.input;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("获取微信openId入参")
@Data
public class GetOpenIdInput {

    @ApiModelProperty(value = "临时登录凭证code",required = true, example = "")
    private String code;

}
