package com.weibei.db.mapper.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.weibei.db.domain.system.CitroenCarNamingModel;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liumeng
 * @since 2021-08-31
 */
@Mapper
public interface CitroenCarNamingMapper extends BaseMapper<CitroenCarNamingModel> {

    List<CitroenCarNamingModel> sqlRead();
}
