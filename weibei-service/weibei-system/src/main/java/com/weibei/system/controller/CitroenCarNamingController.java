package com.weibei.system.controller;


import com.weibei.common.core.domain.Result;
import com.weibei.common.utils.response.ResponseUtils;
import com.weibei.db.domain.system.CitroenCarNamingModel;
import com.weibei.interfaces.service.system.CitroenCarNamingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author liumeng
 * @since 2021-08-31
 */
@RestController
@RequestMapping("sys/test")
@Validated
@Api(value = "测试接口", tags = "测试相关接口")
public class CitroenCarNamingController {

    @Autowired
    private CitroenCarNamingService carNamingService;

    @PostMapping("read")
    @ApiOperation(value = "测试mybatis-plus无sql查询")
    public Result<List<CitroenCarNamingModel>> noSqlRead(){
        return ResponseUtils.ok(carNamingService.read());
    }

    @PostMapping("sql-read")
    @ApiOperation(value = "测试mybatis-plus sql查询")
    public Result<List<CitroenCarNamingModel>> sqlRead(){
        return ResponseUtils.ok(carNamingService.sqlRead());
    }

}

