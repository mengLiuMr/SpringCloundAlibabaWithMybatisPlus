package com.weibei;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import com.weibei.system.annotation.EnableRyFeignClients;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 启动程序
 *
 * @author weibei
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@EnableDiscoveryClient
@EnableRyFeignClients
@EnableAsync
@EnableScheduling
@MapperScan("com.weibei.*.mapper")
public class WeiBeiWebApp
{
    public static void main(String[] args)
    {
        SpringApplication.run(WeiBeiWebApp.class, args);
    }
}
