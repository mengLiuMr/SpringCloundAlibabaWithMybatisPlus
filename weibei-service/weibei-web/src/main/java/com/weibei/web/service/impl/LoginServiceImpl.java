package com.weibei.web.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.weibei.common.core.domain.Constant;
import com.weibei.common.core.domain.Result;
import com.weibei.common.utils.ValidatorUtils;
import com.weibei.db.dto.web.input.GetOpenIdInput;
import com.weibei.db.dto.web.input.LoginAuthViaMobileInput;
import com.weibei.db.dto.web.input.LoginInput;
import com.weibei.db.dto.web.output.LoginOrAuthOutput;
import com.weibei.web.service.LoginService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.regex.Pattern;

@Service
@Slf4j
public class LoginServiceImpl implements LoginService {

    private static final Pattern pattern = Pattern.compile(Constant.PARAM_REGEXRULE_MOBILE);


    public int getWordCount(String s)
    {
        if(ValidatorUtils.checkExistsEmptyFields(s)){
            return 0;
        }

        int length = 0;
        for(int i = 0; i < s.length(); i++)
        {
            int ascii = Character.codePointAt(s, i);
            if(ascii >= 0 && ascii <=255) {
                length++;
            } else {
                length += 2;
            }

        }
        return length;
    }

    @Override
    public Result<LoginOrAuthOutput> loginOrRegistic(LoginAuthViaMobileInput input) throws Exception {
        return null;
    }

    @Override
    public Result loginByWx(LoginInput loginInput) throws Exception {
        return null;
    }

    @Override
    public Result bindMobileByWx(LoginInput loginInput, Long userId) throws Exception {
        return null;
    }

//    private UserToken generateToken(TbUser user) {
//        String token = Constant.LOGIN_TOKEN_PREFIX + Tools.getUUID();
//        UserToken userToken = new UserToken();
//        userToken.setToken(token);
//        userToken.setUpdateTime(new Date());
//        userToken.setExpireTime(DateUtil.addDay(new Date(), 30));
//        userToken.setUserId(user.getId());
//        userToken.setLoginName(user.getNickname());
//        redisUtils.set(Constant.LOGIN_TOKEN_NAME + token, JSONObject.toJSON(user), 12 * 60 * 60);
//        redisUtils.set(Constant.PARAM_FIELD_ACC_ID + user.getId(), token, 12 * 60 * 60);
//        redisUtils.set(Constants.ACCESS_TOKEN + userToken.getToken(), JSONObject.toJSON(userToken), 30 * 24 * 60 * 60L);
//        return userToken;
//    }



    /**
     * 获取微信openid
     * @param input
     * @return
     * @throws Exception
     */
    @Override
    public Result getOpendId(GetOpenIdInput input) throws Exception{
//        if (ValidatorUtils.checkExistsEmptyFields(input.getCode())){
//            return ResponseUtils.badArgument();
//        }
//        String code = input.getCode();
//        String openId = null;
//        JSONObject message;
//        String url = accessTokenUrl + "?appid=" + appId + "&secret=" + appSecret + "&js_code=" + code + "&grant_type=authorization_code";
//        //通过code得到openId
//        RestTemplate restTemplate = new RestTemplate();
//        String response = restTemplate.getForObject(url, String.class);
//        message = JSON.parseObject(response);
//        log.info("###微信授权返回：" + message);
//        if (message.get("openid") != null) {
//            openId = message.get("openid").toString();
//        }
//        GetOpenIdOutput output = new GetOpenIdOutput();
//        output.setOpenId(openId);
//        return ResponseUtils.ok(output);

        return null;
    }

    /**
     * 获取微信信息
     * @param code
     * @return
     * @throws Exception
     */
    private JSONObject getWxInfoByCode(String code) throws Exception{
//        if (StringUtils.isEmpty(code)){
//            return null;
//        }
//        JSONObject message;
//        String url = accessTokenUrl + "?appid=" + appId + "&secret=" + appSecret + "&js_code=" + code + "&grant_type=authorization_code";
//        //通过code得到openId
//        RestTemplate restTemplate = new RestTemplate();
//        String response = restTemplate.getForObject(url, String.class);
//        message = JSON.parseObject(response);
//        log.info("###微信授权返回2：" + message);
//        return message;
        return null;
    }


}
