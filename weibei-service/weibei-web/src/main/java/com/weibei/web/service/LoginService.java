package com.weibei.web.service;

import com.weibei.common.core.domain.Result;
import com.weibei.db.dto.web.input.GetOpenIdInput;
import com.weibei.db.dto.web.input.LoginAuthViaMobileInput;
import com.weibei.db.dto.web.input.LoginInput;
import org.springframework.transaction.annotation.Transactional;


@Transactional
public interface LoginService{
    Result loginOrRegistic(LoginAuthViaMobileInput loginAuthViaMobileInput) throws Exception;

    Result loginByWx(LoginInput loginInput) throws Exception;

    Result bindMobileByWx(LoginInput loginInput, Long userId) throws Exception;

    /**
     * 获取微信openid
     * @param input
     * @return
     * @throws Exception
     */
    Result getOpendId(GetOpenIdInput input) throws Exception;

}
