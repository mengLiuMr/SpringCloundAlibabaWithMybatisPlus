package com.weibei.web.util;

import org.springframework.util.StringUtils;

import java.math.BigDecimal;

public class DecimalFormatUtils {
    public static BigDecimal formatterBigDecimal(BigDecimal b){
        if(StringUtils.isEmpty(b)){
            return new BigDecimal(0).setScale(0);
        }
        else {
            if(b.compareTo(b.setScale(0,BigDecimal.ROUND_DOWN))==0){
                return b.setScale(0,BigDecimal.ROUND_DOWN);
            }
            return b.setScale(2,BigDecimal.ROUND_HALF_UP);
        }
    }
}
