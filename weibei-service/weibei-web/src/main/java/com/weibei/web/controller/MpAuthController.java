package com.weibei.web.controller;

import com.weibei.common.core.domain.Result;
import com.weibei.common.utils.response.ResponseUtils;
import com.weibei.db.dto.web.input.GetOpenIdInput;
import com.weibei.db.dto.web.input.LoginAuthViaMobileInput;
import com.weibei.db.dto.web.input.LoginInput;
import com.weibei.db.dto.web.output.GetOpenIdOutput;
import com.weibei.db.dto.web.output.LoginOrAuthOutput;
import com.weibei.web.service.LoginService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 登录注册接口
 *
 * @author weibei
 */
@RestController
@RequestMapping("mp/auth")
@Validated
@Api(value = "登录或注册相关接口", tags = "登录或注册相关接口")
@Slf4j
public class MpAuthController
{
    @Autowired
    private LoginService loginService;
    /**
     * 登录或注册
     * @param loginAuthViaMobileInput
     * @return
     */
    @ApiOperation(value = "小程序手机登录或注册", notes = "小程序手机登录或注册")
    @PostMapping("loginOrRegistic")
    public Result<LoginOrAuthOutput> loginOrRegistic(@RequestBody LoginAuthViaMobileInput loginAuthViaMobileInput) {
        Result<LoginOrAuthOutput> result;
        try {
            result = loginService.loginOrRegistic(loginAuthViaMobileInput);
        }catch (Exception e){
            e.printStackTrace();
            log.error("loginOrRegistic error", e);
            result = ResponseUtils.serverError();
        }
        return result;

    }

    /**
     * 微信授权登录并注册
     * @param loginInput
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "小程序微信登录注册", notes = "小程序微信登录注册")
    @PostMapping("loginByWx")
    public Object loginByWx(@RequestBody LoginInput loginInput) throws Exception{
        Object result;
        try {
            result = loginService.loginByWx(loginInput);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("loginByWx error" , e);
            result = ResponseUtils.serverError();
        }
        return result;
    }


    /**
     * 获取微信openid
     * @param input
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "获取微信openid", notes = "获取微信openid")
    @PostMapping("getOpenId")
    public Result<GetOpenIdOutput> getOpenId(@RequestBody GetOpenIdInput input) throws Exception{
        Result result;
        try {
            result = loginService.getOpendId(input);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("getOpenId error" , e);
            result = ResponseUtils.serverError();
        }
        return result;
    }

}
