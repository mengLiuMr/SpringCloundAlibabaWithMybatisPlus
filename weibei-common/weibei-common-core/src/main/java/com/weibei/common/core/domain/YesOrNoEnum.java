package com.weibei.common.core.domain;

/**
* @Author: liumeng
* @Date: 2021/5/14
*/
public enum YesOrNoEnum {
    NO(0,"是"),
    YES(1,"否");


    private Integer code;
    private String message;

    YesOrNoEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
    public String getMessage() {
        return message;
    }
    public Integer getCode() {
        return code;
    }
}
