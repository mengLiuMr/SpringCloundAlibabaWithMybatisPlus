package com.weibei.common.core.domain;

/**
* @Author: liumeng
* @Date: 2021/5/14
*/
public enum PartnerGiftTypeEnum {
    INTEGRAL_GIFT(0,"积分礼包"),
    DRIVE_GIFT(1,"用车礼包"),
    CUMULATIVE_TEST_DRIVE_NEW(2,"累计试驾(ONE众风神计划)"),
    CUMULATIVE_DEAL_NEW(3,"累计成交(ONE众风神计划)");


    private Integer code;
    private String message;

    PartnerGiftTypeEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
    public String getMessage() {
        return message;
    }
    public Integer getCode() {
        return code;
    }
}
