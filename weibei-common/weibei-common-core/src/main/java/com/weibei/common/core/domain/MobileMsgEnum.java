package com.weibei.common.core.domain;

public enum MobileMsgEnum {
    //您的验证码为：##code##,该验证码5分钟内有效，请勿泄漏于他人。
    REGISTER("REGISTER","您的验证码为：##code##,该验证码5分钟内有效，请勿泄漏于他人。"),
    LOGIN("LOGIN","您的验证码为：##code##,该验证码5分钟内有效，请勿泄漏于他人。"),
    AUTH("AUTH","您的验证码为：##code##,该验证码5分钟内有效，请勿泄漏于他人。"),
    CTCC("CTCC","您的验证码为：##code##,该验证码30分钟内有效，请勿泄漏于他人。"),
    PAY("PAY","您的验证码为：##code##,该验证码5分钟内有效，请勿泄漏于他人。"),
    SECKILL("SECKILL","您的验证码为：##code##,该验证码5分钟内有效，请勿泄漏于他人。"),
    CALC("CALC","您的验证码为：##code##,该验证码5分钟内有效，请勿泄漏于他人。"),
    CLIA("CLIA","您的验证码为：##code##,该验证码5分钟内有效，请勿泄漏于他人。"),
    NOTICYTICKET("NOTICYTICKET","您的卡券为：##ticketNo##，卡密为：##ticketPw##，请勿泄漏于他人。");


    public static String getMessage(String type){
        MobileMsgEnum[] msgEnums = MobileMsgEnum.values();
        for (MobileMsgEnum msgEnum : msgEnums) {
            if(msgEnum.getType().equals(type)) {
                return msgEnum.getMessage();
            }
        }
        return "";
    }

    private String type;
    private String message;

    MobileMsgEnum(String type, String message) {
        this.type = type;
        this.message = message;
    }
    public String getMessage() {
        return message;
    }
    public String getType() {
        return type;
    }
}
