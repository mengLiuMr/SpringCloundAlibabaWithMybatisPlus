package com.weibei.common.core.domain;

import org.springframework.context.annotation.Configuration;

import java.math.BigDecimal;

@Configuration
public class Constant {
    public static String LOGIN_TOKEN_NAME = "X-web.Token";
    public static String LOGIN_USER = "loginUser";
    public static String LOGIN_TOKEN_PREFIX = "GRAIN:TOKEN:";
    // 参数字段
    // 参数字段
    public static String PARAM_FIELD_PHONE = "phone";
    public static String PARAM_FIELD_VALIDATION_CODE = "validationCode";
    public static String PARAM_FIELD_NICKNAME = "nickname";
    public static String PARAM_FIELD_LOGIN_PASSWORD = "loginPassword";
    public static String PARAM_FIELD_INVITE_CODE = "inviteCode";
    public static String PARAM_FIELD_MOBILE = "mobile";
    public static String PARAM_FIELD_MSG_TYPE = "msgType";
    public static String PARAM_FIELD_MSG_CODE = "msgCode";
    public static String PARAM_FIELD_USER_ID = "userId";
    public static String PARAM_FIELD_USER_ASSET_ID = "userAssetId";
    public static String PARAM_FIELD_STATUS = "status";
    public static String PARAM_FIELD_NEWLOGINPWD = "newLoginPwd";
    public static String PARAM_FIELD_NEWOPTPWD = "newOptPwd";
    public static String PARAM_FIELD_SET_UPDATE = "setOrUpdate";
    public static String PARAM_FIELD_CONFIRMED_PWD = "confirmedPwd";
    public static String PARAM_FIELD_OPTPWD = "optPwd";
    public static String PARAM_FIELD_PRODUCT_ID = "productId";
    public static String PARAM_FIELD_MODEL_ID = "modelId";
    public static String PARAM_FIELD_QR_ADDR = "qrAddr";
    public static String PARAM_FIELD_ACC_NAME = "accName";
    public static String PARAM_FIELD_ACC_TYPE = "accType";
    public static String PARAM_FIELD_WITHDRAW_TYPE = "withdrawType";
    public static String PARAM_FIELD_BANK_USERNAME = "bankUserName";
    public static String PARAM_FIELD_BANK_NUMBER = "bankNumber";
    public static String PARAM_FIELD_REMARK = "remark";
    public static String PARAM_FIELD_ACC_ID = "accId";
    public static String PARAM_FIELD_NAME = "name";
    public static String PARAM_FIELD_IMAGE_NETWORK_ADDR = "imageNetworkAddr";
    public static String PARAM_FIELD_ID_CARD = "idCard";
    public static String PARAM_FIELD_ID_CARD_FRONT = "idCardFront";
    public static String PARAM_FIELD_ID_CARD_BACK = "idCardBack";
    public static String PARAM_FIELD_INHAND_PHOTO = "inHandPhoto";
    public static String PARAM_FIELD_DEL_SHOPPING_CARTS = "delShoppingCarts";
    public static String PARAM_FIELD_LOG_MODULE = "logModule";
    public static String PARAM_FIELD_OPERATOR_TYPE = "operatorType";
    public static String PARAM_FIELD_MENU_CODE = "menuCode";
    public static String PARAM_FIELD_PAGE_SIZE = "pageSize";
    public static String PARAM_FIELD_PAGE_NUM = "pageNum";
    public static String PARAM_FIELD_TOTAL_PRICE = "totalPrice";
    public static String PARAM_FIELD_IP = "ip";
    public static String PARAM_FIELD_TOTAL_QUANTITIES = "totalQuantities";
    public static String PARAM_FIELD_ORDER_ID = "referenceId";
    public static String PARAM_FIELD_ORDERID = "orderId";
    public static String SHOPPING_CART_PREFIX = "tct_shopping_cart_";
    public static String PARAM_FIELD_GOODS_ID = "goodsId";
    public static String PARAM_FIELD_SPECIFICATION_ID = "specificationId";
    public static String PARAM_FIELD_ORDER_GOODS_ID = "orderGoodsId";
    public static String PARAM_FIELD_REFUND_TYPE = "refundType";
    public static String PARAM_FIELD_REFUND_REASON = "refundReason";
    public static String PARAM_FIELD_REFUND_AMOUNT = "refundAmount";
    public static String PARAM_FIELD_REFUND_REMARK = "refundRemark";
    public static String PARAM_FIELD_RECIVE_NAME = "reciveName";
    public static String PARAM_FIELD_RECIVE_PHONE = "recivePhone";
    public static String PARAM_FIELD_RECIVE_ADDR = "reciveAddr";
    public static String PARAM_FIELD_PROOF_URL = "proofUrl";
    public static String PARAM_FIELD_AMOUNT = "amount";
    public static String PARAM_FIELD_OUT_TRADE_NO = "out_trade_no";
    public static String PARAM_FIELD_NOTIFY_URL = "notify_url";

    public static String PARAM_FIELD_COMMENT_POST_TYPE = "comment_post_type";

    public static String PARAM_FIELD_RECIPIENT_ID = "accId";
    public static String PARAM_FIELD_ALI_ACCOUNT = "aliAccount";
    public static String PARAM_FIELD_BANK_NAME = "bankName";
    public static String ACC_TYPE_WEICHAT = "WEICHAT";
    public static String ACC_TYPE_ALIPAY = "ALIPAY";
    public static String ACC_TYPE_BANK = "BANK";


    public static String PARAM_FIELD_REFUND_ID = "refundId";
    public static String PARAM_FIELD_LOGISTICS_NUM = "logisticsNum";
    public static String PARAM_FIELD_LOGISTICS_COMPANY = "logisticsCompany";
    public static String PARAM_FIELD_LOGISTICS_MOBILE = "logisticsMobile";

    public static String ADDRESS_PERMISSION_AREA = "community_delivery_area";
    public static String AUTH_OPT_PSW_CHECK_PREFIX = "opt_psw_check_";
    //资产简称
    public static String ASSET_BALANCE = "BALANCE";//余额
    public static String ASSET_INTEGRAL = "INTEGRAL";//积分
    public static String ASSET_AWARD_BALANCE = "ABALANCE";//收益余额


    public static String ASSET_EXT_CASH = "EXT_CASH";//资产操作类型

    //参数名
    public static String PARAM_CONTINUOUS_SIGN_AWARD = "continuous_sign_award";
    public static String PARAM_SIGN_AWARD_PER_DAY = "sign_award_per_day";
    public static String PARAM_COMMUNITY_DELIVERY_AREA = "community_delivery_area";
    public static String PARAM_GOLDEN_CARD_INTEGRAL = "golden_card_integral";
    public static String PARAM_DIAMOND_CARD_INTEGRAL = "diamond_card_integral";
    public static String PARAM_MINI_TEAM_AWARD_PCT = "mini_team_award_pct";
    public static String PARAM_MANAGE_AWARD_AMOUNT = "manage_award_amount";
    public static String PARAM_DEFAULT_TRADING_AMOUNT = "default_trading_amount";

    //正则表达式
    public static String PARAM_REGEXRULE_MOBILE = "[0-9]*";

    //车型和对应的图片
    public static String PARAM_STYLE_XR = "奕炫";
    public static String PARAM_STYLE_AX7 = "2020款AX7";
    public static String PARAM_STYLE_XR_PIC = "https://hongtai-public.obs.cn-east-2.myhuaweicloud.com/mini-program/hc20200421/dffsxy-f01.jpg";
    public static String PARAM_STYLE_AX7_PIC = "https://hongtai-public.obs.cn-east-2.myhuaweicloud.com/mini-program/hc20200421/dffs2020ax7-f01.png";

    //经销商是否显示粉丝
    public static String AGENCY_RANK_ISVIEW_RANKNUM = "agency.rank.isview.ranknum";

    //经销商是否显示下订数
    public static String AGENCY_RANK_ISVIEW_ORDER = "agency.rank.isview.order";

    //经销商是否显示留资数
    public static String AGENCY_RANK_ISVIEW_CAPITAL = "agency.rank.isview.capital";

    //车型配置字典类型id
    public static String AGENCY_RANK_ISVIEW_TYPEID = "2"; //配置
    public static String AGENCY_RANK_ISVIEW_TYPEPARAID = "3"; //参数

    //车型颜色设置，颜色字典类型
    public static String PARAM_CAR_COLOR_TYPEID = "1"; //配置

    //核销码默认失效时间
    public static Integer PARAM_COUPON_DEFAULT_EXP_TIME = 30;

    //逗号分隔符
    public static String PARAM_NOT_SEP = ",";

    //文章类型
    public static Integer PARAM_ARTICLE_PRAISE_TYPE = 0;
    public static Integer PARAM_ARTICLE_INFO_TYPE = 1;

    //banner详情类型
    public static Integer PARAM_BANNER_TYPE_ARTICLE_PRAISE = 4;
    public static Integer PARAM_BANNER_TYPE_ARTICLE_INFO = 5;

    //媒体评测和车主口碑默认头像和发帖人
    public static String PARAM_DEF_SYS_AUTHOR = "东风风神";
    public static String PARAM_DEF_SYS_AVATAR = "https://e-mallfile.dfpv.com.cn/mini-program/img/FSV2/df20200526161159.png";

    //礼包原始积分配置
    public static BigDecimal PARAM_GIFT_ONE_INTEGRAL = new BigDecimal(5010);
    public static BigDecimal PARAM_GIFT_TWO_INTEGRAL = new BigDecimal(15020);
    public static BigDecimal PARAM_GIFT_THREE_INTEGRAL = new BigDecimal(25030);
    //任务描述
    public static String PARAM_GIFT_INVITE_DESC = "邀请好友注册";
    public static String PARAM_GIFT_CLUE_DESC = "有效线索";
    public static String PARAM_GIFT_BEST_ARTICLE_DESC = "汽车之家精华帖";
    public static String PARAM_GIFT_CLUE_FINISH_DESC = "线索成交";

    //风神合伙人标识ICON
    public static String PARAM_PARTNER_ICON = "https://e-mallfile.dfpv.com.cn/mini-program/img/FSV2/df5293.png";
    public static String PARAM_PARTNER_INVITE_AWARD_INTEGRAL = "PARAM_PARTNER_INVITE_AWARD_INTEGRAL";
    public static String PARAM_PARTNER_CLUE_AWARD_INTEGRAL = "PARAM_PARTNER_CLUE_AWARD_INTEGRAL";
    public static String PARAM_PARTNER_FINISH_CLUE_AWARD_INTEGRAL = "PARAM_PARTNER_FINISH_CLUE_AWARD_INTEGRAL";

    //小程序不需要验证码的手机号登录接口用于MD5加密的原串
    public static String PARAM_MD5STR_LONGIN_BY_MOBILE_NOCODE = "nsH3hLmGSDqTnDFJ";

    //普通好车类型（奕炫，AX7）退款开始时间
    public static String PARAM_REFUND_STARTTIME_NORMALL = "2020-06-30 23:59:59";
    //普通好车类型（奕炫，AX7）查询开始时间
    public static String PARAM_REFUND_QUERY_STARTTIME = "2020-06-30 23:59:59";
    //奕炫GS退款开始时间
    public static String PARAM_REFUND_STARTTIME_YXGS = "2020-06-30 23:59:59";

    //车型编码
    public static String PARAM_CARCODE_YX = "YX";
    public static String PARAM_CARCODE_AX7 = "AX7";
    public static String PARAM_CARCODE_YXGS = "YXGS";

    //重名后添加后缀
    public static String PARAM_RENAME_FIX = "-副本";

    //CTCC竞猜排名得分情况
    public static BigDecimal PARAM_CTCC_NO1 = BigDecimal.valueOf(100);
    public static BigDecimal PARAM_CTCC_NO2 = BigDecimal.valueOf(90);
    public static BigDecimal PARAM_CTCC_NO3 = BigDecimal.valueOf(80);
    public static BigDecimal PARAM_CTCC_NO4 = BigDecimal.valueOf(70);
    public static BigDecimal PARAM_CTCC_NO5 = BigDecimal.valueOf(60);
    public static BigDecimal PARAM_CTCC_NO6 = BigDecimal.valueOf(50);
    public static BigDecimal PARAM_CTCC_NO7 = BigDecimal.valueOf(40);
    public static BigDecimal PARAM_CTCC_NO8 = BigDecimal.valueOf(30);
    public static BigDecimal PARAM_CTCC_OTHER = BigDecimal.valueOf(20);
    //CCTC显示获奖名单条数
    public static Integer PARAM_CTCC_PRIZE_VIEW_LIMIT_DEF = 8;

    //注册抽奖卡券相关
    public static String PARAM_LOTTERY_DRAW_TICKET_NAME = "核销码";
    public static String PARAM_LOTTERY_DRAW_TICKET_GIFT_NAME = "到店礼包";
    //卡券类型 1-注册抽奖核销码类型
    public static Integer PARAM_LOTTERY_DRAW_TICKET_TYPE = 1;
    public static String PARAM_LOTTERY_DRAW_STARTTIME_KEY = "LOTTERY_DRAW_START_TIME";
    //注册抽奖车型字典表对应的key
    public static String PARAM_LOTTERY_DRAW_CARTYPE_DICT_KEY = "lottery_car_type";

    //默认头像
    public static String PARAM_DEFAULT_AVATAR = "https://e-mallfile.dfpv.com.cn/mini-program/img/partner/tempImg/partner_avatar.jpg";
    //头像地址的前缀
    public static String PARAM_AVATAR_PREFIX_1 = "https://thirdwx.qlogo.cn";
    public static String PARAM_AVATAR_PREFIX_2 = "https://wx.qlogo.cn";
    public static String PARAM_AVATAR_PREFIX_3 = "https://e-mallfile.dfpv.com.cn";


    //抽奖随机签名串
    public static String PARAM_SIGN_STR = "vnAnRlgGYOz32wXb";

    /** 安全配置相关 **/
    public static String PARAM_SECURITY_SIGN_STR = "zKNGE6YT08wI47My";
    public static String PARAM_IP_BLACK = "IP_BLACK";
    public static String PARAM_URL_WHITE = "URL_WHITE";
    //白名单
    public static String PARAM_IP_WHITE = "IP_WHITE";
    //ip访问次数
    public static String PARAM_IP_REQ_TIMES = "IP_REQ_TIMES_";
    //ip访问次数限制的值
    public static String PARAM_IP_REQ_LIMIT = "IP_REQ_LIMIT";

    //整车下订抽奖随机签名加密串
    public static String PARAM_CARS_RAFFLE_SIGN = "vjUnN7HDRsl1Nxfc";

    //整车下订判断订单是否能抽奖随机签名加密串
    public static String PARAM_CARS_ABLE_TO_RAFFLE_SIGN = "KOjNHbacOc6AGss8";

    //下订抽奖推送消息接口授权token存放在redis中的key,有效期2小时
    public static String PARAM_PUSH_ACCESS_TOKEN = "PUSH_ACCESS_TOKEN";

    //短信验证码接口地址
    public static String PARAM_SEND_CODE_URL = "/web/mp/msg/sendMsgCode";

    //dmp待同步数据在redis中的key
    public static String PARAM_DMP_WAIT_SYNC_KEY = "dmp_sync_data";

    //crm线索重要字段（存在字典表里）
    public static String PARAM_CRM_CLUE_FILED_KEY = "crm_clue_field_drive";
    public static String PARAM_CRM_CLUE_CHANNER1 = "dfpv_source_channel1";
    public static String PARAM_CRM_CLUE_CHANNER3 = "dfpv_source_channel3";
    public static String PARAM_CRM_CLUE_SOURCE_NAME = "source_name";
    public static String PARAM_CRM_CLUE_TYPE = "type";

    //风神合伙人字典车型列表key
    public static String PARAM_PARTNER_CAR_DROPDOWN = "partner_kv_cars_type";

    //小程序提交线索页面路由(通过风神合伙人kv图跳转)
    public static String PARAM_PATRNER_CLUE_PAGE = "pages/activityCar/activityCar";
    //小程序提交线索页面路由(通过风神合伙人有效线索跳转)
    public static String PARAM_PATRNER_CLUE_PAGE_OLD = "pages/vaildClew/vaildClew";

    //首页路由
    public static String PARAM_FOUND_PAGE = "pages/found/found";
    //风神合伙人留资页路由
    public static String PARAM_ADDCLUE_PAGE = "subcontractA_df/pages/fsPartner/vaildClew_new/vaildClew_new";


    //砍价记录失效时间，分钟为单位
    public static String PARAM_BARGAIN_RECORD_EXPDATE = "bargain_record_expdate";

    //京东积分兑相关
    public static final String RESULT_SUCCESS = "success";
    public static final String PAY_ORDERING_ERROR_CODE = "0011";
    public static final String RESULT_RESULT_CODE = "resultCode";
    public static final String DEDUCE_POINTS_SUCCESS = "paySuccess";
    public static final String DEDUCE_POINTS_SUCCESS_TIPS = "支付成功!";
    public static final String DEDUCE_POINTS_ERROR_TIPS = "支付异常，请联系管理员!";
    public static final String DEDUCE_POINTS_NO_BALANCE_TIPS = "余额不足!";

    //小程序和APP用户积分检查相关
    public static final String CHECK_INTEGRAL_MP_NOMU_TIPS = "小程序手机号和UionId都为空";
    public static final String CHECK_INTEGRAL_MP_NOA_TIPS = "小程序资产为空";
    public static final String CHECK_INTEGRAL_APP_CN_ERR_TIPS = "查询APP积分异常";
    public static final String CHECK_INTEGRAL_APP_RT_ERR_TIPS = "查询APP积分返回异常";
    public static final String CHECK_INTEGRAL_NO_EQ_TIPS = "小程序和APP积分不等";
    public static final String CHECK_INTEGRAL_APP_USER_ERR_TIPS = "查询APP用户信息异常";
    public static final String CHECK_INTEGRAL_MPAPP_NOM_TIPS = "小程序和APP都没有手机号";
    public static final String CHECK_INTEGRAL_MP_NOM_ERR_TIPS = "小程序没有手机号APP有手机号";

    //红包雨相关常量
    public static final String HBY_SIGN_PREFIX  = "pdzbvTV8RCZKccNd";
    public static final String HBY_CONFIG_NUM_PREFIX  = "hby_config_";
    public static final String HBY_ALREADY_PREFIX  = "hby_already_";
    public static final String HBY_SHARE_PREFEX="hby:share:";
    public static final Integer HBY_SHARE_INTEGRAL=50;
    //长侧招募
    public static final String CCZM_GEIALIKE_NUM_YXMAX  = "geialike_num_yxmax";

    //新版合伙人相关配置的key
    public static final String PARTNER_SIGNIN_CONFIG  = "partner_signin_config";
    public static final String PARTNER_SHARE_CONFIG  = "partner_share_config";
    public static final String PARTNER_ADDCLUE_CONFIG  = "partner_addclue_config";
    public static final String PARTNER_TESTDRIVE_INTEGRAL  = "partner_testdrive_integral";
    public static final String MIDDLE_REGIST_CONFIG  = "middle_regist_config";
    public static final String MIDDLE_SIGNIN_CONFIG  = "middle_signin_config";
    public static final String MIDDLE_SHARE_CONFIG  = "middle_share_config";
    public static final String MIDDLE_ADDCLUE_CONFIG  = "middle_addclue_config";
    public static final String MIDDLE_TESTDRIVE_INTEGRAL  = "middle_testdrive_integral";
    public static final String PARTNER_V2_CLUELIST_SHOWTIME  = "2021-01-01";
    //todo 上生产时需要改成02-01
    public static final String PARTNER_V2_STARTTIME  = "2021-02-01";
    public static final String PARTNER_SIGNIN_ENDTIME_INIT  = "2021-02-28 23:59:59";

    //v2合伙人阶梯奖励配置
    public static final String PARTNER_TEST_DRIVE_PRIZE1  = "partner_test_drive_prize1";//合伙人累计试驾
    public static final String PARTNER_CLUE_PRIZE1  = "partner_clue_prize1";//合伙人累计成交
    public static final String PARTNER_TEST_DRIVE_PRIZE2  = "partner_test_drive_prize2";//经纪人累计试驾
    public static final String PARTNER_CLUE_PRIZE2  = "partner_clue_prize2";//经纪人累计成交
    public static final String PARTNER_CHANNEL  = "partner_channel";//合伙人渠道

    public static final String CRM_CHANNEL_1  = "微信生态";//一级来源
    public static final String CRM_CHANNEL_2  = "微信小程序（商城）";//二级来源
    public static final String CRM_CHANNEL_2_1 = "一店一码";//二级来源

    public static final String SYSTEM_ENVIRONMENT_CONFIG = "system_environment_config";//系统环境配置（dev开发环境，prod线上环境，线上环境可不配置）

    public static final String DEV_CLUE_INFO = "{\"XML\":\"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\"?><root><row><HAS_OPPTY>Y</HAS_OPPTY><HAS_ORDER>Y</HAS_ORDER><INTENTION_TIME>05/16/2021 16:42:41</INTENTION_TIME><CROSSTOWN_TIME>05/17/2022 00:00:00</CROSSTOWN_TIME><REASON></REASON></row></root>\",\"error_spcCode\":\"?\",\"error_spcMessage\":\"?\",\"object_spcId\":\"?\",\"process_spcInstance_spcId\":\"1-AQ0T83\",\"siebel_spcOperation_spcObject_spcId\":\"?\"}";
    /**
     * 退款审核
     */
    public static final String RETURN_APPROVE_PREFIX="RETURN:APPROVE:";
    //核销时间的最大值
    public static final int EXPIRE_YEAR=2038;
    //设置活动的抽奖次数
    public static final String WIN_RANK="WIN:RANK:";
    //默认答题次数
    public static final int FREQUENCY=1;
    //答题次数的key
    public static final String NUM_ANSWER="NUM:ANSWER:";
    //答题人数限制
    public static final String ANS_NUM="ans.num";
    //奕行空间对接可售后的商品id
    public static final Long GOOD_ID=1149L;
    public static final Long GOOD_NO_RETURN_ID =1107L;
    public static final Long YX_CAR_ID_NO_RETURN=1178L;
    public static final Long YX_CAR_ID_RETURN=1177L;

    //欧洲杯redis存储的时间10天
    public static final Long QUIZ_EXPIRE=100*24*60*60L;
    //限购人数
    public static final String LIMIT_QUIZ="LIMIT:QUIZ:";
    //注册开关
    public static final String REGIST_SWITCH="regist.switch";
    //竞猜的人数限制
    public static final int LIMIT_QUIZ_NUM=3;
    //竞猜的次数
    public static final String QUIZ_USRT="QUIZ:USER:";
    //按照天超时
    public static final Long DAY_EXPIRE=24*60*60L;
    /**
     * 游戏限制人数
     */
    public interface Game{
        //默认游戏次数
        String GAME_CS="game.cs";
        //游戏第一关限制人数
        String GAME_LIMIT_NUM="game.limitNum";
        //游戏第二关限制人数
        String GAME_LIMIT_2_NUM="game.limit2Num";
        //int GAME_LIMIT_NUM=550;
        //游戏通关人数限制
        String GAME_OVER_NUM="game.overNum";
        //int GAME_OVER_NUM=200;
        //游戏注册领取积分人数
        String GAME_REFIST_NUM="game.registNum";
        //int GAME_REFIST_NUM=200;
        //排名(针对于游戏排名的前两个阶段)
        String GAME_RANK_LIMIT="GAME:RANK:LIMIT:";
        //排名(针对于游戏排名的通关)
        String GAME_RANK_OVER="GAME:RANK:OVER:";
        //游戏排名
        String GAME_RANK="GAME:RANK:";
        //redis 用户抽奖次数
        String GAME_USER_CS="GAME:USER:";
        //第一个阶段存储用户信息
        String GAME_LIMIT_1="GAME:LIMIT:1:";
        //第二个阶段存储用户信息
        String GAME_LIMIT_2="GAME:LIMIT:2:";
        //存储通关的用户信息
        String GAME_OVER="GAME:OVER:";
        //防止重复提交
        String GAME="GAME:";
        //游戏注册人数
        String REGIST="GAME:REGIST:";
        //游戏分享
        String GAME_SHARE="game.share";
        //分享限制人数
        String GAME_SHARE_NUM="game.shareNum";
        //每个人分享的人数
        String GAME_SHARE_LIMIT_NUM="GAME:SHARE:LIMIT";
    }
}
