package com.weibei.common.utils.jd;

import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @Author: maochenfei
 * @Date: 2019/4/17
 * @Description:
 */
public class SignatureUtil {
    public static String genSrcData(TreeMap<String, Object> paramMap, List<String> unSignKeyList) {
        StringBuilder sb = new StringBuilder();
        Iterator result = unSignKeyList.iterator();

        while (result.hasNext()) {
            String iterator = (String) result.next();
            paramMap.remove(iterator);
        }

        Iterator iterator1 = paramMap.entrySet().iterator();

        while (iterator1.hasNext()) {
            Map.Entry result1 = (Map.Entry) iterator1.next();
            if (result1.getValue() != null && result1.getValue().toString().trim().length() > 0) {
                sb.append(result1.getKey() + "=" + result1.getValue() + "&");
            }
        }

        String result2 = sb.toString();
        if (result2.endsWith("&")) {
            result2 = result2.substring(0, result2.length() - 1);
        }
        return result2;
    }

    public static String signString(Object object, List<String> unSignKeyList) throws IllegalArgumentException, IllegalAccessException {
        TreeMap map = objectToMap(object);
        return genSrcData(map, unSignKeyList);
    }

    public static TreeMap<String, Object> objectToMap(Object object) throws IllegalArgumentException, IllegalAccessException {
        TreeMap map = new TreeMap();

        for (Class cls = object.getClass(); cls != Object.class; cls = cls.getSuperclass()) {
            Field[] fields = cls.getDeclaredFields();
            Field[] var7 = fields;
            int var6 = fields.length;

            for (int var5 = 0; var5 < var6; ++var5) {
                Field f = var7[var5];
                f.setAccessible(true);
                map.put(f.getName(), f.get(object));
            }
        }

        return map;
    }
    /**
     * 验证签名是否正确
     * @param s1
     * @param signData
     * @param rsaSignPubKey
     * @return
     */
    public static boolean verifySign(String s1, String signData, String rsaSignPubKey) {
        boolean flag;
        if(signData != null && signData.length()!= 0) {
            if(rsaSignPubKey != null && rsaSignPubKey.length() != 0) {
                try {
                    String s2 = ShaUtil.encrypt(s1, "SHA-256");

                    byte[] signByte = RsaUtil.decryptBASE64(signData);
                    byte[] decryptArr = RsaUtil.decryptByPublicKey(signByte, rsaSignPubKey);
                    String decryptStr = new String(decryptArr);
                    if(s2.equals(decryptStr)) {
                        flag = true;
                        return flag;
                    } else {
                        throw new RuntimeException("Signature verification failed.");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    throw new RuntimeException("verify signature failed.", e);
                }
            } else {
                throw new IllegalArgumentException("Argument \'rsaSignPubKey\' is null or empty");
            }
        } else {
            throw new IllegalArgumentException("Argument \'signData\' is null or empty");
        }
    }
    public static String genSign(String s1,String rsaSignPrivateKey) throws Exception {

        String s2 = ShaUtil.encrypt(s1, "SHA-256");
        byte[] s2Rsa = RsaUtil.encryptByPrivateKey(s2.getBytes("UTF-8"), rsaSignPrivateKey);
        String sign = RsaUtil.encryptBASE64(s2Rsa);
        return sign;
    }
}
