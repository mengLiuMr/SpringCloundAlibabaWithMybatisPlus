package com.weibei.common.valid;

import com.weibei.common.annotation.EnumValid;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;

/**
 * @author daixilong
 * @version 1.0.0
 * @ClassName EnumIntegerValid.java
 * @Description TODO 定义枚举int类型校验器
 * @Date 2021/5/27 10:05
 */
public class EnumIntegerValid implements ConstraintValidator<EnumValid,Integer> {

    public String in;

    @Override
    public void initialize(EnumValid constraintAnnotation) {
        in=constraintAnnotation.in();
    }

    @Override
    public boolean isValid(Integer value, ConstraintValidatorContext context) {
        if (Objects.isNull(value)) {
            return true;
        }
        return in.contains(value.toString());
    }
}
