package com.weibei.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author daixilong
 * @version 1.0.0
 * @ClassName CrmEnum.java
 * @Description TODO 维护Crm线索，暂时用活动的首字母
 * @Date 2021/5/26 16:16
 */
@Getter
@AllArgsConstructor
public enum CrmEnum {
    JQLS("车展活动总部","专项活动","东风风神奕炫驾趣联赛");
    private String sourceChannel1;
    private String sourceChannel2;
    private String sourceName;
}
