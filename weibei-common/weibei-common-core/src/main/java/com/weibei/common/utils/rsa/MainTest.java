package com.weibei.common.utils.rsa;

public class MainTest {
    private static final String publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCr9khx/sMJCummlnXor2Kdo3Sc3d1m1JRJQiXh1z4kODQf4kKShXBoKzQQotxwmUA1L1Ng4iDY1pMbG7KfpF6yqpteH+XiQQJpGUFIR85Exj0CEn74kZgxG5zq0D3xEKEv/1Fd7XQRYaK7cAg8Ml/hpVVih4j8C+L7csoAaaWbVQIDAQAB";

    public static void main(String[] args) throws Exception {
        String filepath="D:/work/";

        //生成公钥和私钥文件
//        RSAEncrypt.genKeyPair(filepath);

//        System.out.println("--------------公钥加密私钥解密过程-------------------");
//        String plainText="ihep_公钥加密私钥解密";
//        //公钥加密过程
//        byte[] cipherData=RSAEncrypt.encrypt(RSAEncrypt.loadPublicKeyByStr(RSAEncrypt.loadPublicKeyByFile(filepath)),plainText.getBytes());
//        String cipher=Base64.encode(cipherData);
//        //私钥解密过程
//        byte[] res=RSAEncrypt.decrypt(RSAEncrypt.loadPrivateKeyByStr(RSAEncrypt.loadPrivateKeyByFile(filepath)), Base64.decode(cipher));
//        String restr=new String(res);
//        System.out.println("原文："+plainText);
//        System.out.println("加密："+cipher);
//        System.out.println("解密："+restr);
//        System.out.println();
//
//        System.out.println("--------------私钥加密公钥解密过程-------------------");
////        plainText="ihep_私钥加密公钥解密";
////        //私钥加密过程
////        cipherData=RSAEncrypt.encrypt(RSAEncrypt.loadPrivateKeyByStr(RSAEncrypt.loadPrivateKeyByFile(filepath)),plainText.getBytes());
////        cipher=Base64.encode(cipherData);
////        //公钥解密过程
////        res=RSAEncrypt.decrypt(RSAEncrypt.loadPublicKeyByStr(RSAEncrypt.loadPublicKeyByFile(filepath)), Base64.decode(cipher));
////        restr=new String(res);
////        System.out.println("原文："+plainText);
////        System.out.println("加密："+cipher);
////        System.out.println("解密："+restr);
////        System.out.println();

        String publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCt3VPJq6Y66nNDivdgXVoSubOiuF0VVzcg0gYwHOW7Tj0diDyQaGHINMv8LNk1wZg58yzP9H4zWWRMsgbCEIqB8Vz3AWuTG+RKupBBE3Zc/vbgrYYHEgUDhJrPslP9Oqhscz5tqKjBekiJfXnaod9k6jZr2cTXcdW06hzWJ0q4YwIDAQAB";
        String privateKey = "MIICeAIBADANBgkqhkiG9w0BAQEFAASCAmIwggJeAgEAAoGBAIwJs2ZBzKQY5MMGLJ+KK8NnFF3GirwExWEVgrylkePyAoXxwMQfO4txK0M/Z5vAd0VBZRjChkpCDezjnj2SeMrNjMOQq3dMahXuvOXP1ZXmzhsP5KILPUuL03OChQrrWRYemMSiZPO1ocwZcOO3qhluGUaw4KoyFl6lX+SQTvnnAgMBAAECgYAF45KLxlCYxgM4wFW4D9NvUDJnDyZeacTzJDhvPnc4IsFAaL42gSm733J2TYBJ6Hobi3h45hVHoid7wsawyCwUK0O2KixIM7Z70YRObd7DrlGoR6Q86XGgCEhro5bXsI3+gfHdQ4IzDuLHK0XxTZCaUolirO+A0dfEo5SqL57kQQJBAMnItJf4ScBlroBh2BTLvPxbkpm5rnL6OCoCOYaJeBcvZdqMHC7dHhnHOgf8kOQYciQ/cguZVvWO70PBIxWYrF8CQQCxqerBe5iN/z7uh6UfPzT111qj0hlWQvBHZRyhdv8BtRumeffWCsESlIcaRLYTq6wy130ddZsxU+4kMVrneh95AkEAizUxCgrb7qT3r1BRP3+TYNJYID/Ojzn611hzkVJzjCNB9HVm2BnqM0STz4C6APKwhDtVFZVzPbOfc7Hh33tVpwJBAKT4qdNF6E+afx5Q+IP+Vjgd472Q3xQZjjJNcnuAGqTEPkukn/yx+bw+DQHYYdX8KCcBr90GEriIPe5/ofCllskCQQCC/dbgq6zOa0wWw3T1TcS2g/Cy6EBZ19HvmanVATT4eeo/wmq0bQUN0FuzSM23hexf3/4zKintGZ+KgInhAB62";


        System.out.println("---------------私钥签名过程------------------");
        String content="ihep_这是用于签名的原始数据";
//        String signstr= RSASignature.sign(content, RSAEncrypt.loadPrivateKeyByFile(filepath));
        String signstr= RSASignature.sign(content, privateKey);
        System.out.println("签名原串："+content);
        System.out.println("签名串："+signstr);
        System.out.println();

        System.out.println("---------------公钥校验签名------------------");
        System.out.println("签名原串："+content);
        System.out.println("签名串："+signstr);

//        System.out.println("验签结果："+ RSASignature.doCheck(content, signstr, RSAEncrypt.loadPublicKeyByFile(filepath)));
        System.out.println("验签结果："+ RSASignature.doCheck(content, signstr, publicKey));
        System.out.println();

    }
}