package com.weibei.common.enums;

/**
 * 数据源
 * 
 * @author weibei
 */
public enum DataSourceType
{
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
