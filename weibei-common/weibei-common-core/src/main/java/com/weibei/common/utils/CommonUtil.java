package com.weibei.common.utils;

import java.util.Objects;

/**
 * @ClassName
 * @Description TODO
 * @Author liumeng
 * @Date 2021/5/15 19:57
 **/
public class CommonUtil {

    /**
     * 拼接合伙人成交礼品id
     * @param id
     * @return
     */
    public static final Long dealGiftId(Object id){
//        return Objects.isNull(id) ? null : Long.valueOf(Constant.DEAL_NUM_PREFIX+""+id);
        return Objects.isNull(id) ? null : Long.valueOf(id+"");
    }

    /**
     * 拼接合伙人邀约试驾礼品id
     * @param id
     * @return
     */
    public static final Long testDriveGiftId(Object id){
//        return Objects.isNull(id) ? null : Long.valueOf(Constant.TEST_DRIVE_NUM_PREFIX+""+id);
        return Objects.isNull(id) ? null : Long.valueOf(id+"");
    }
}
