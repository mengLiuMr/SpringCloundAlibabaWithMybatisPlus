package com.weibei.common.core.domain;

/**
* @Author: liumeng
* @Date: 2021/5/14
*/
public enum EnvironmentEnum {
    DEV("dev","测试环境"),
    PROD("prod","生产环境");


    private String code;
    private String message;

    EnvironmentEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }
    public String getMessage() {
        return message;
    }
    public String getCode() {
        return code;
    }
}
