package com.weibei.common.utils.kuaiDi100.outPut;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @Author: api.kuaidi100.com
 * @Date: 2020-07-14 16:07
 */
@ApiModel("物流信息返回")
@Data
public class QueryTrackResp {
    /**
     * 消息体，请忽略
     */
    private String message;
    /**
     * 快递单号
     */
    @ApiModelProperty(value = "快递单号", example = "")
    private String nu;
    /**
     * 是否签收标记
     */
    @ApiModelProperty(value = "是否签收标记", example = "")
    private String ischeck;
    /**
     * 	快递公司编码,一律用小写字母
     */
    @ApiModelProperty(value = "快递公司编码", example = "")
    private String com;

    @ApiModelProperty(value = "快递公司名称", example = "")
    private String comName;
    /**
     * 通讯状态
     */
    @ApiModelProperty(value = "通讯状态", example = "")
    private String status;
    /**
     * 轨迹详情数组
     */
    @ApiModelProperty(value = "轨迹详情", example = "")
    private List<QueryTrackData> data;
    /**
     * 快递单当前状态，包括0在途，1揽收，2疑难，3签收，4退签，5派件，6退回，7转投，10待清关，11清关中，12已清关，13清关异常，14拒签 等13个状态
     */
    @ApiModelProperty(value = "快递单当前状态，包括0在途，1揽收，2疑难，3签收，4退签，5派件，6退回，7转投，10待清关，11清关中，12已清关，13清关异常，14拒签 等13个状态", example = "")
    private String state;
    /**
     * 快递单明细状态标记
     */
    @ApiModelProperty(value = "快递单明细状态标记", example = "")
    private String condition;

    @ApiModelProperty(value = "位置移动信息", example = "")
    private QueryTrackRouteInfo routeInfo;
    /**
     * 查不到轨迹或者其他问题返回码
     */
    @ApiModelProperty(value = "查不到轨迹或者其他问题返回码", example = "")
    private String returnCode;
    /**
     * 查不到轨迹或者其他问题返回结果
     */
    @ApiModelProperty(value = "查不到轨迹或者其他问题返回结果", example = "")
    private boolean result;
}
