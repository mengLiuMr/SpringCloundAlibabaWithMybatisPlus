package com.weibei.common.exception;


import com.weibei.common.core.domain.RetCodeEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * @author daixilong
 * @version 1.0.0
 * @ClassName ParamCheckException.java
 * @Description TODO 定义参数校验异常
 * @Date 2021/5/26 14:08
 */
@Setter
@Getter
public class ParamCheckException extends RuntimeException {
    private int code;
    private String msg;

    private ParamCheckException(){}

    public ParamCheckException(int code,String msg){
        this.code=code;
        this.msg=msg;
    }

    public ParamCheckException(String msg){
        this.code=RetCodeEnum.CODE_401.getCode();
        this.msg=msg;
    }


    public static void throwMessage(int code, String message) {
        throw new ParamCheckException(code, message);
    }

    public static void throwMessage(String message) {
        throw new ParamCheckException(RetCodeEnum.CODE_401.getCode(), message);
    }
}
