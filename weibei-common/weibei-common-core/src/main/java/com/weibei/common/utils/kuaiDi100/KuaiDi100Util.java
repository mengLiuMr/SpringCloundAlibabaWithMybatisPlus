package com.weibei.common.utils.kuaiDi100;

import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.weibei.common.core.domain.HttpClient;
import com.weibei.common.utils.kuaiDi100.inPut.QueryTrackParam;
import com.weibei.common.utils.kuaiDi100.outPut.AutoNumResp;
import com.weibei.common.utils.kuaiDi100.outPut.QueryTrackResp;
import com.weibei.common.utils.security.Md5Utils;
import okhttp3.FormBody;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class KuaiDi100Util {

    private static String customer = "DBF5FA70C8C18DC132587028ADD0112E";


    private static String key = "nryzEvaT9026";

    //实时查询订单接口
    private static String realTimeQueryUrl = "https://poll.kuaidi100.com/poll/query.do";

    //智能识别单号接口 ?num=%s&key=%s
    private static String autoComUrl = "http://www.kuaidi100.com/autonumber/auto";



    public QueryTrackResp queryTrack(QueryTrackParam trackParam) throws Exception {
        if (trackParam == null) {
            return null;
        }
        QueryTrackResp queryTrackResp = new QueryTrackResp();
        String params = JSONObject.toJSONString(trackParam);
        String sign = Md5Utils.hash(params+key+customer).toUpperCase();
        System.out.println(sign);
        FormBody formBody = new FormBody.Builder()
                .add("customer", customer)
                .add("sign", sign)
                .add("param",params)
                .build();
        System.out.println(params);
        System.out.println(JSONObject.toJSONString(trackParam));
        Map<String,Object> response = HttpClient.postRequest5(realTimeQueryUrl, formBody);
        System.out.println(response);
        if ("200".equals(response.get("status").toString())
                && StringUtils.isNotBlank(response.get("data").toString())){
            queryTrackResp = new Gson().fromJson(response.get("data").toString(),QueryTrackResp.class);
        }
        return queryTrackResp;
    }

    public List<AutoNumResp> queryComNum(String trackingNo) throws Exception {
        if (StringUtils.isBlank(trackingNo)) {
            return null;
        }
        FormBody formBody = new FormBody.Builder()
                .add("num", trackingNo)
                .add("key", key)
                .build();
        Map<String,Object> response = HttpClient.postRequest5(autoComUrl, formBody);
        System.out.println(response);
        if ("200".equals(response.get("status").toString())
                && StringUtils.isNotBlank(response.get("data").toString())){
            return new Gson().fromJson(response.get("data").toString(),new TypeToken<List<AutoNumResp>>(){}.getType());
        }
        return null;
    }

//    public templates void main(String[] agrs){
//        QueryTrackParam trackParam = new QueryTrackParam();
//        trackParam.setCom("yunda");
//        trackParam.setNum("4308631283942");
//        trackParam.setFrom("");
//        trackParam.setPhone("");
//        trackParam.setResultv2("0");
//        trackParam.setOrder("desc");
//        trackParam.setShow("0");
//        trackParam.setTo("");
//        try {
//            QueryTrackResp trackResp = queryTrack(trackParam);
//
//            System.out.println(trackResp);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
}
