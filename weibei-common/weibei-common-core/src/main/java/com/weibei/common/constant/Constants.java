package com.weibei.common.constant;

/**
 * 通用常量信息
 *
 * @author weibei
 */
public class Constants
{
    /**
     * UTF-8 字符集
     */
    public static final String UTF8             = "UTF-8";

    /**
     * 通用成功标识
     */
    public static final String SUCCESS          = "0";

    /**
     * 通用失败标识
     */
    public static final String FAIL             = "1";

    /**
     * 登录成功
     */
    public static final String LOGIN_SUCCESS    = "Success";

    /**
     * 注销
     */
    public static final String LOGOUT           = "Logout";

    /**
     * 登录失败
     */
    public static final String LOGIN_FAIL       = "Error";

    /**
     * 自动去除表前缀
     */
    public static final String AUTO_REOMVE_PRE  = "true";

    /**
     * 当前记录起始索引
     */
    public static final String PAGE_NUM         = "pageNum";

    /**
     * 每页显示记录数
     */
    public static final String PAGE_SIZE        = "pageSize";

    /**
     * 排序列
     */
    public static final String ORDER_BY_COLUMN  = "sortField";

    /**
     * 排序的方向 "desc" 或者 "asc".
     */
    public static final String IS_ASC           = "sortOrder";

    public static final String CURRENT_ID       = "current_id";

    public static final String CURRENT_USERNAME = "current_username";

    public static final String TOKEN            = "token";

    public static final String DEFAULT_CODE_KEY = "random_code_";

    public final static String ACCESS_TOKEN     = "access_token_";

    public final static String ACCESS_USERID    = "access_userid_";

    public static final String RESOURCE_PREFIX  = "/profile";

    public static final String REMOTE_DAY_IP  = "REMOTE_DAY_IP_";
    public static final String REMOTE_DAY_IP_LIMIT = "REMOTE_DAY_IP_LIMIT";
    public static final String REMOTE_HOUR_IP  = "REMOTE_HOUR_IP_";
    public static final String REMOTE_HOUR_IP_LIMIT  = "REMOTE_HOUR_IP_LIMIT";

    public static final String MOBILE_DAY_IP  = "MOBILE_DAY_IP_";
    public static final String MOBILE_DAY_IP_LIMIT  = "MOBILE_DAY_IP_LIMIT";
    public static final String MOBILE_HOUR_IP  = "MOBILE_HOUR_IP_";
    public static final String MOBILE_HOUR_IP_LIMIT  = "MOBILE_HOUR_IP_LIMIT";
    /**
     * 奕行调用key
     */
    public static final String YIXING_KEY="yixing";
    /**
     * 支付回调奕行key
     */
    public static final String YIXING_BACK_KEY="25a7c41961fd970ea61cb43e873ac0d6";
    /**
     * 红包雨分享获取奖励
     */
    public static final String HBY_SHARE_INTEGRAL="hby.share.integral";
    /**
     * 红包雨站内信模板
     */
    public static final String MSG_CONTENT_TEMP="感谢您参与“{}”活动,恭喜您获得{}";
    /**
     * 红包雨分享注册内容
     */
    public static final String MSG_SHARE_CONTENT_TEMP="感谢您参与“{}”活动,您又获得{}积分";
    /**
     * 红包雨分享注册标题
     */
    public static final String MSG_SHARE_TITLE="惊喜礼包";
    /**
     * 红包雨开关
     */
    public static final String HBY_SWITCH="hby.switch";
    /**
     * 小游戏标题
     */
    public static final String GAME_TITLE="幸福全家桶，分享有大礼";
    /**
     * 小游戏内容
     */
    public static final String GAME_CONTENT="激动的心，颤抖的手，快来领取你的积分！您的{}好友已成功注册，恭喜您获得{}积分。";
    /**
     * 欧洲杯竞猜标题
     */
    public static final String QUIZ_TITLE="中奖提醒";
    /**
     * 欧洲杯竞猜内容
     */
    public static final String QUIZ_CONTENT="恭喜您在欧洲杯竞猜活动中获得500积分！";
    /**
     * @description :夏日限定提交
     */
    public static final String SUMMER_SUBMIT="SUMMER_SUBMIT";

    public static final String WX_PUSH_REDIS_QUEUE="WX_PUSH_REDIS_QUEUE";
}
