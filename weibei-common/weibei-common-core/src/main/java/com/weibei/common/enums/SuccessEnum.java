package com.weibei.common.enums;

/**
 * 失败与否
 *
 * @author weibei
 */
public enum SuccessEnum
{
    SUC(0, "成功"), FAIL(1, "失败");

    private final Integer code;
    private final String info;

    SuccessEnum(Integer code, String info)
    {
        this.code = code;
        this.info = info;
    }

    public Integer getCode()
    {
        return code;
    }

    public String getInfo()
    {
        return info;
    }
}
