package com.weibei.common.core.domain;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.util.List;

/**
 * @program: plat_cloud
 * @description: 分页返回输出对象
 * @author: leizhiming
 * @date: 2020-05-09 10:49
 **/
@Data
public class PageInfoResult<T> {

    private List<T> rows;

    private Integer pageNum;

    private Integer pageSize;

    @JsonSerialize(using = ToStringSerializer.class)
    private Long total;
}
