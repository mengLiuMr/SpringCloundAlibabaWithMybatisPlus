package com.weibei.common.utils.kuaiDi100.outPut;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: api.kuaidi100.com
 * @Date: 2020-07-14 16:14
 */
@ApiModel("位置移动信息")
@Data
public class QueryTrackRouteInfo {
    /**
     * 出发位置
     */
    @ApiModelProperty(value = "出发位置", example = "")
    private QueryTrackPosition from;
    /**
     * 当前位置
     */
    @ApiModelProperty(value = "当前位置", example = "")
    private QueryTrackPosition cur;
    /**
     * 收货地
     */
    @ApiModelProperty(value = "收货地", example = "")
    private QueryTrackPosition to;
}
