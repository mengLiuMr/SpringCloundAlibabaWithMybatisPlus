package com.weibei.common.enums;

import java.util.*;

/**
 * 流水记录枚举
 */
public enum TransactionTypeEnum {

    INTEGRAL_PARTNER(101, "风神合伙人", 1, "INTEGRAL", 1, 1, "风神合伙人", 1),
    INTEGRAL_CONSUME(102, "购物消费", 1, "INTEGRAL", 2, 2, "购物消费", 2),
    INTEGRAL_PARTNER_INVITE(103, "邀请好友注册", 1, "INTEGRAL", 1, 3, "邀请好友注册", 3),
    INTEGRAL_PARTNER_ARTICLE(104, "汽车之家精华帖", 1, "INTEGRAL", 1, 4, "汽车之家精华帖", 4),
    INTEGRAL_PARTNER_CLUE(105, "线索成交", 1, "INTEGRAL", 1, 5, "线索成交", 5),
    INTEGRAL_PARTNER_INTERACT(106, "互动积分", 1, "INTEGRAL", 1, 6, "互动积分", 6),
    INTEGRAL_APP_SYNC_ADD(107, "APP同步增加", 1, "INTEGRAL", 1, 7, "APP同步增加", 7),
    INTEGRAL_APP_SYNC_SUB(108, "APP同步减少", 1, "INTEGRAL", 2, 8, "APP同步减少", 8),
    INTEGRAL_ORDER_REFUND(109, "订单退还", 1, "INTEGRAL", 1, 9, "订单退还", 9),
    INTEGRAL_LOTTERY_DRAW(110, "注册抽奖", 1, "INTEGRAL", 1, 10, "注册抽奖", 10),
    INTEGRAL_CARS_ORDER_RAFFLE(111, "下订抽奖", 1, "INTEGRAL", 1, 11, "下订抽奖", 11),
    INTEGRAL_JDJFD_REDUCE(112, "京东积分兑扣减", 1, "INTEGRAL", 2, 12, "京东积分兑扣减", 12),
    INTEGRAL_JDJFD_ADD(113, "京东积分兑返还", 1, "INTEGRAL", 1, 13, "京东积分兑返还", 13),
    INTEGRAL_JDJFD_ER_HANDLE_INC(114, "京东积分兑误扣积分增加", 1, "INTEGRAL", 1, 14, "京东积分兑误扣积分增加", 14),
    INTEGRAL_JDJFD_ER_HANDLE_DEC(115, "京东积分兑误返积分扣减", 1, "INTEGRAL", 1, 15, "京东积分兑误返积分扣减", 15),
    INTEGRAL_ORDER_RAFFLE_INC(116, "下订抽奖增加积分", 1, "INTEGRAL", 1, 16, "下订抽奖增加积分", 16),
    INTEGRAL_HBY_INC(117, "红包雨", 1, "INTEGRAL", 1, 17, "下订抽奖增加积分", 17),
    INTEGRAL_BLIND_BOX(118, "盲盒抽奖", 1, "INTEGRAL", 1, 18, "盲盒抽奖", 18),
    INTEGRAL_LEAVE_INFO(119, "留资抽奖", 1, "INTEGRAL", 1, 19, "留资抽奖", 19),
    INTEGRAL_SIGNIN(120, "签到奖励", 1, "INTEGRAL", 1, 20, "签到奖励", 20),
    INTEGRAL_SHARE_1(121, "活动趣闻", 1, "INTEGRAL", 1, 21, "活动趣闻", 21),
    INTEGRAL_TESTDRIVE(122, "试驾奖励", 1, "INTEGRAL", 1, 22, "试驾奖励", 22),
    INTEGRAL_VALID_CLUE(123, "有效线索", 1, "INTEGRAL", 1, 23, "有效线索", 23),
    INTEGRAL_BUY_INTEGRAL_GOODS(124, "购买积分充值", 1, "INTEGRAL", 1, 24, "购买积分充值", 24),
    INTEGRAL_INTEGRAL_GOODS_REFUND(125, "购买积分充值-退款", 1, "INTEGRAL", 2, 25, "购买积分充值-退款", 25),
    INTEGRAL_SHARE_2(126, "发圈美图", 1, "INTEGRAL", 1, 26, "每日一圈", 26),
    INTEGRAL_SHARE_3(127, "发圈文案", 1, "INTEGRAL", 1, 27, "发圈文案", 27),
    INTEGRAL_ADDCLUE(128, "留资奖励", 1, "INTEGRAL", 1, 28, "留资奖励", 28),
    INTEGRAL_MIDDLE_REGIST(129, "经纪人注册礼", 1, "INTEGRAL", 1, 29, "经纪人注册", 29),
    INTEGRAL_DRAW_LEAVE(130, "抽奖留资", 1, "INTEGRAL", 1, 30, "抽奖留资", 30),
    INTEGRAL_TEST_DRIVE(131, "累计试驾奖励", 1, "INTEGRAL", 1, 5, "累计试驾奖励", 31),
    INTEGRAL_CAR_FINISH(132, "累计成交奖励", 1, "INTEGRAL", 1, 5, "累计成交奖励", 32),
    INTEGRAL_SHARE_HBY(133,"红包雨分享奖励",1,"INTEGRAL",1,33,"红包雨分享奖励",33),
    INTEGRAL_QUIZ(134,"欧洲杯竞猜",1,"INTEGRAL",1,34,"欧洲杯竞猜",34),
    INTEGRAL_QUIZ_ANS(135,"欧洲杯快问快答",1,"INTEGRAL",1,35,"欧洲杯快问快答",35),
    INTEGRAL_GAME_INVIT_SHARE(136,"分享幸福MAX全家桶合成",1,"INTEGRAL",1,36,"分享幸福MAX全家桶合成",36),
    INTEGRAL_GAME_SHARE(137,"幸福MAX全家桶合成",1,"INTEGRAL",1,37,"幸福MAX全家桶合成",37),
    INTEGRAL_ORDER_RAFFLE_P_INC(138, "下单抽奖增加积分", 1, "INTEGRAL", 1, 38, "下单抽奖增加积分", 38),
    INTEGRAL_SUMMER_OLYMPIC(139,"夏日限定-镜头下的奥运",1,"INTEGER",1,39,"夏日限定-镜头下的奥运",39),
    ;

    /* 编码 */
    public int code;
    /* 描述 */
    public String desc;
    /* 类目编号 */
    public int moduleCode;
    /* 类目 */
    public String module;
    /* 增加或者减少 1增加2减少 */
    public int inOrOut;
    /* 菜单编号 */
    public int menuCode;
    /* 菜单描述 */
    public String menuDesc;
    /* 排序 */
    public int sort;

    public static String getDesc(int code) {
        TransactionTypeEnum[] transactionTypeEnums = TransactionTypeEnum.values();
        for (TransactionTypeEnum transactionTypeEnum : transactionTypeEnums) {
            if (transactionTypeEnum.code == code) {
                return transactionTypeEnum.desc;
            }
        }
        return "";
    }

    public static TransactionTypeEnum getTransactionTypeEnum(int code) {
        TransactionTypeEnum[] transactionTypeEnums = TransactionTypeEnum.values();
        for (TransactionTypeEnum transactionTypeEnum : transactionTypeEnums) {
            if (transactionTypeEnum.code == code) {
                return transactionTypeEnum;
            }
        }
        return null;
    }

    public static String getMenuDesc(int code) {
        TransactionTypeEnum[] transactionTypeEnums = TransactionTypeEnum.values();
        for (TransactionTypeEnum transactionTypeEnum : transactionTypeEnums) {
            if (transactionTypeEnum.code == code) {
                return transactionTypeEnum.menuDesc;
            }
        }
        return "";
    }

    public static String getModule(int code) {
        TransactionTypeEnum[] transactionTypeEnums = TransactionTypeEnum.values();
        for (TransactionTypeEnum transactionTypeEnum : transactionTypeEnums) {
            if (transactionTypeEnum.moduleCode == code) {
                return transactionTypeEnum.module;
            }
        }
        return "";
    }

    public static int getModuleCode(int operatorCode) {
        TransactionTypeEnum[] transactionTypeEnums = TransactionTypeEnum.values();
        for (TransactionTypeEnum transactionTypeEnum : transactionTypeEnums) {
            if (transactionTypeEnum.code == operatorCode) {
                return transactionTypeEnum.moduleCode;
            }
        }
        return 0;
    }

    public static int getModuleCodeByModuleDesc(String moduleDesc) {
        TransactionTypeEnum[] transactionTypeEnums = TransactionTypeEnum.values();
        for (TransactionTypeEnum transactionTypeEnum : transactionTypeEnums) {
            if (transactionTypeEnum.module.equalsIgnoreCase(moduleDesc)) {
                return transactionTypeEnum.moduleCode;
            }
        }
        return 0;
    }

    public static int getInOrOut(int operatorCode) {
        TransactionTypeEnum[] transactionTypeEnums = TransactionTypeEnum.values();
        for (TransactionTypeEnum transactionTypeEnum : transactionTypeEnums) {
            if (transactionTypeEnum.code == operatorCode) {
                return transactionTypeEnum.inOrOut;
            }
        }
        return 0;
    }

    private static List<Map<String, Object>> setupMenu(Map<Integer, Object> menusMap) {
        List<Map<String, Object>> typesList = new ArrayList<>();
        Map<String, Object> types;
        for (Map.Entry<Integer, Object> typeMap : menusMap.entrySet()) {
            types = new HashMap<>(2);
            types.put("menuCode", typeMap.getKey());
            types.put("menuDesc", typeMap.getValue());
            typesList.add(types);
        }
        return typesList;
    }

    private static void sortMenu(TransactionTypeEnum[] transactionTypeEnum) {
        int length = transactionTypeEnum.length;
        int preIndex;
        TransactionTypeEnum current;
        for (int i = 1; i < length; i++) {
            preIndex = i - 1;
            current = transactionTypeEnum[i];
            while (preIndex >= 0 && transactionTypeEnum[preIndex].sort > current.sort) {
                transactionTypeEnum[preIndex + 1] = transactionTypeEnum[preIndex];
                preIndex--;
            }
            transactionTypeEnum[preIndex + 1] = current;
        }
    }

    public static List<Integer> getOperatorTypes(int moduleCode, int menuCode) {
        List<Integer> types = new ArrayList<>();
        TransactionTypeEnum[] transactionTypeEnums = TransactionTypeEnum.values();
        for (TransactionTypeEnum transactionTypeEnum : transactionTypeEnums) {
            if (transactionTypeEnum.moduleCode == moduleCode && transactionTypeEnum.menuCode == menuCode) {
                types.add(transactionTypeEnum.code);
            }
        }
        return types;
    }

    TransactionTypeEnum(int code, String desc, int moduleCode, String module, int inOrOut, int menuCode, String menuDesc, int sort) {
        this.code = code;
        this.desc = desc;
        this.moduleCode = moduleCode;
        this.module = module;
        this.inOrOut = inOrOut;
        this.menuCode = menuCode;
        this.menuDesc = menuDesc;
        this.sort = sort;
    }

}
