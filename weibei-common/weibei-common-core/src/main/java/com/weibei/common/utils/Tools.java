package com.weibei.common.utils;

import com.weibei.common.core.domain.Result;

import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @Auther: lk
 * @Date: 2018/6/26 11:51
 * @Description:
 */
public class Tools {

    /**
     * 手机号码校验
     *
     * @param phone
     * @return
     */
    public static Result isPhone(String phone) {
        Result rs = new Result();
        String regex = "^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\\d{8}$";
        if (phone.length() != 11) {
            rs.setErrmsg("手机号应为11位数");
            rs.setSuccess(false);
            return rs;
        } else {
            Pattern p = Pattern.compile(regex);
            Matcher m = p.matcher(phone);
            boolean isMatch = m.matches();
            if (!isMatch) {
                rs.setErrmsg("请填入正确的手机号");
                rs.setSuccess(false);
                return rs;
            }
            return rs;
        }
    }

    /**
     *     获取uuid
     * @return
     */
    public static String getUUID() {
        return UUID.randomUUID().toString().replace("-", "").substring(0, 15);
    }

    public static void main(String[] arg){
        System.out.println("FSMP#" +getUUID().toUpperCase());
    }
}

