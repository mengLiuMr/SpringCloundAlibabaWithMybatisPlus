package com.weibei.common.utils;

import cn.hutool.core.lang.Assert;
import com.weibei.common.exception.ParamCheckException;

/**
 * @author daixilong
 * @version 1.0.0 断言处理
 * @ClassName Assert.java
 * @Description TODO
 * @Date 2021/5/26 14:16
 */
public class AssertUtil {
    public static void isTrue(boolean expression,String message){
        Assert.isTrue(expression, ()->new ParamCheckException(message));
    }

    public static void isTrue(boolean expression,int code,String message){
        Assert.isTrue(expression, ()->new ParamCheckException(code,message));
    }

    public static void isFalse(boolean expression, String message) {
        Assert.isFalse(expression, ()->new ParamCheckException(message));
    }

    public static void isFalse(boolean expression, int code,String message) {
        Assert.isFalse(expression, ()->new ParamCheckException(code,message));
    }

    public static void isNull(Object object, String message){
        Assert.isNull(object,()->new ParamCheckException(message));
    }

    public static void isNull(Object object,int code ,String message){
        Assert.isNull(object,()->new ParamCheckException(code,message));
    }

    public static void notNull(Object object, String message){
        Assert.notNull(object,()->new ParamCheckException(message));
    }

    public static void notNull(Object object, int code,String message){
        Assert.notNull(object,()->new ParamCheckException(code,message));
    }
}
