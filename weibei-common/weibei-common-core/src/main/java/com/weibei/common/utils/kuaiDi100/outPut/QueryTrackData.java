package com.weibei.common.utils.kuaiDi100.outPut;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: api.kuaidi100.com
 * @Date: 2020-07-14 16:13
 */
@ApiModel("轨迹详情")
@Data
public class QueryTrackData {
    /**
     * 时间，原始格式
     */
    @ApiModelProperty(value = "时间，原始格式", example = "")
    private String time;
    /**
     * 物流轨迹节点内容
     */
    @ApiModelProperty(value = "物流轨迹节点内容", example = "")
    private String context;
    /**
     * 格式化后时间
     */
    @ApiModelProperty(value = "格式化后时间", example = "")
    private String ftime;
    /**
     * 行政区域的编码
     */
    @ApiModelProperty(value = "行政区域的编码", example = "")
    private String areaCode;
    /**
     * 行政区域的名称
     */
    @ApiModelProperty(value = "行政区域的名称", example = "")
    private String areaName;
    /**
     * 签收状态 (0在途，1揽收，2疑难，3签收，4退签，5派件，6退回，7转投)
     */
    @ApiModelProperty(value = "签收状态 (0在途，1揽收，2疑难，3签收，4退签，5派件，6退回，7转投)", example = "")
    private String status;
}
