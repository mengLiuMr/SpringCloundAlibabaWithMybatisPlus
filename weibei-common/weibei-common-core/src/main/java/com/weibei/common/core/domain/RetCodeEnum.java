package com.weibei.common.core.domain;

public enum RetCodeEnum {
    CODE_200(200,"成功"),
    CODE_401(401,"参数不可以为空"),
    CODE_402(402,"用户信息不存在"),
    CODE_403(403,"库存不足"),
    CODE_501(501,"非法登录或者token已失效,请重新登录"),
    CODE_406(406,"验证错误"),
    CODE_405(405,"参数错误"),
    CODE_408(408,"余额不足"),
    CODE_409(409,"该地址暂不支持购买"),
    CODE_410(410,"支付失败"),
    CODE_411(411,"重复请求"),
    CODE_407(407,"请输入有效验证码"),
    CODE_412(412,"不存在此请求"),
    CODE_413(413,"核销码不存在"),
    CODE_414(414,"已超期"),
    CODE_415(415,"手机号已注册"),
    CODE_416(416,"已经是同一个账号，无需合并"),
    CODE_417(417,"订单不存在"),
    CODE_418(418,"不能重复收藏"),
    CODE_419(419,"订单无法取消"),
    CODE_420(420,"订单无法确认收货"),
    CODE_421(421,"已经评价过了"),
    CODE_422(422,"手机号未绑定"),
    CODE_423(423,"该用户已经领取过了"),
    CODE_424(424,"未找到领取记录"),
    CODE_425(425,"核销码状态非待核销状态，不可以核销"),
    CODE_426(426,"网点不匹配，不可以核销"),
    CODE_427(427,"收藏商品信息不存在"),
    CODE_428(428,"没有红包雨信息"),
    CODE_429(429,"业务错误返回信息"),
    CODE_430(430,"参与组队的人员已满"),
    CODE_431(431,"异常"),
    CODE_500(500,"服务错误, 请联系服务提供商"),
    CODE_999(999,"签名错误"),
    ;
    private int code;
    private String desc;
    RetCodeEnum(int code, String desc){
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
