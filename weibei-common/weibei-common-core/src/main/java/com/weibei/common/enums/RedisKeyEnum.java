package com.weibei.common.enums;

/**
 * redis key 管理
 */
public enum RedisKeyEnum {


    GET_ONE_LEVEL_LIST("getOneLevelList", "获取一级类目", 1 * 24 * 60 * 60L),
    GET_TWO_LEVEL_LIST("getTwoLevelList", "获取二级类目", 1 * 24 * 60 * 60L),
    GET_VIRTUAL_GOODS_LIST("getVirtualGoodsList", "虚拟好物列表(每个二级目录展示四个商品)", 1 * 24 * 60 * 60L),
    GET_VIRTUAL_GOODS_DETAILS("getVirtualGoodsDetail", "虚拟好物详情", 1 * 24 * 60 * 60L),
    GET_PRACTICAL_GOODS_LIST("getPracticalGoodsList", "实物好物列表(每个二级目录展示四个商品)", 1 * 24 * 60 * 60L),
    GET_PRACTICAL_GOODS_DETAILS("getPracticalGoodsDetails", "实物好物详情", 1 * 24 * 60 * 60L),
    GET_USER_ORDER_LOGISTICS_INFO("USER_ORDER_LOGISTICS_", "订单物流信息查询次数", 1 * 24 * 60 * 60L)
;
    RedisKeyEnum(String key, String desc, Long expirtTime) {
        this.key = key;
        this.desc = desc;
        this.expirtTime = expirtTime;
    }

    private String key;
    private String desc;
    private Long expirtTime;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Long getExpirtTime() {
        return expirtTime;
    }

    public void setExpirtTime(Long expirtTime) {
        this.expirtTime = expirtTime;
    }
}
