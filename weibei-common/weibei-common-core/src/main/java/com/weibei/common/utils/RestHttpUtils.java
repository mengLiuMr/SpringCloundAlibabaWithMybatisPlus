package com.weibei.common.utils;

import cn.hutool.http.HttpRequest;
import lombok.extern.slf4j.Slf4j;

/**
 * @author daixilong
 * @version 1.0.0
 * @ClassName httpUtil.java
 * @Description TODO 可以参考Hutool
 * @Date 2021/6/1 19:30
 */
@Slf4j
public class RestHttpUtils {

    private RestHttpUtils(){}
    /**
     *功能描述 json格式的post-http调用
     * @author daixilong
     * @date 2021/6/1
     * @param param
     * @Param url
     * @return java.lang.String
     */
    public static String postJson(String url, String param){
        log.info("远程调用方法地址:{}",url);
        log.info("远程调用传参信息:{}",param);
        HttpRequest post = cn.hutool.http.HttpUtil.createPost(url);
        String body = post.contentType("application/json").body(param).execute().body();
        log.info("远程调用结果信息:{}",body);
        return body;
    }
}
