package com.weibei.common.enums;

/**
 * banner位置
 * 
 * @author weibei
 */
public enum BannerPositonEnum
{
    BANNER_1(1, "发现页banner"),
    BANNER_2(2, "商城-好车"),
    BANNER_3(3, "商城-好物"),
    BANNER_6(6, "选车页banner"),
    BANNER_10(10, "风神合伙人KV图"),
    BANNER_11(11, "粉丝福利"),
    BANNER_12(12, "精彩活动"),
    BANNER_13(13, "预约试驾kv"),
    ;

    private final int code;
    private final String info;

    BannerPositonEnum(int code, String info)
    {
        this.code = code;
        this.info = info;
    }

    public int getCode()
    {
        return code;
    }

    public String getInfo()
    {
        return info;
    }
}
