package com.weibei.common.utils;

import java.text.Collator;
import java.util.Comparator;

public class SortChineseName implements Comparator<String> {
    Collator cmp = Collator.getInstance(java.util.Locale.CHINA);
    @Override
    public int compare(String o1, String o2) {
        if (cmp.compare(o1, o2)>0){
            return 1;
        }else if (cmp.compare(o1, o2)<0){
            return -1;
        }
        return 0;
    }
}
