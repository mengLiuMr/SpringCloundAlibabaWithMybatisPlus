package com.weibei.common.utils.response;

import com.weibei.common.core.domain.Result;
import com.weibei.common.core.domain.RetCodeEnum;

/**
 * 响应操作结果
 *
 * <pre>
 *  {
 *      errno： 错误码，
 *      errmsg：错误消息，
 *      data：  响应数据
 *  }
 * </pre>
 *
 * <p>
 * 错误码：
 * <ul>
 * <li>0，成功；
 * <li>4xx，前端错误，说明前端开发者需要重新了解后端接口使用规范：
 * <ul>
 * <li>401，参数错误，即前端没有传递后端需要的参数；
 * <li>402，参数值错误，即前端传递的参数值不符合后端接收范围。
 * </ul>
 * <li>5xx，后端错误，除501外，说明后端开发者应该继续优化代码，尽量避免返回后端错误码：
 * <ul>
 * <li>501，验证失败，即后端要求用户登录；
 * <li>502，系统内部错误，即没有合适命名的后端内部错误；
 * <li>503，业务不支持，即后端虽然定义了接口，但是还没有实现功能；
 * <li>504，更新数据失效，即后端采用了乐观锁更新，而并发更新时存在数据更新失效；
 * <li>505，更新数据失败，即后端数据库更新失败（正常情况应该更新成功）。
 * </ul>
 * <li>6xx，小商城后端业务错误码， 具体见Dts-admin-api模块的AdminResponseCode。
 * <li>7xx，管理后台后端业务错误码， 具体见Dts-wx-api模块的WxResponseCode。
 * </ul>
 */
public class ResponseUtils<T> {

	public static <T> Result<T> ok() {
		Result<T> result = new Result<T>();
		result.setErrno(0);
		result.setErrmsg("成功");
		result.setSuccess(true);
		return result;
	}

	public static <T> Result<T> ok(String message) {
		Result<T> result = new Result<T>();
		result.setErrno(0);
		result.setErrmsg(message);
		result.setSuccess(true);
		return result;
	}

	public static <T> Result<T> ok(T data) {
		Result<T> result = new Result<T>();
		result.setErrno(0);
		result.setErrmsg("成功");
		result.setData(data);
		result.setSuccess(true);
		return result;
	}

	public static <T> Result<T> ok(T data,String message) {
		Result<T> result = new Result<T>();
		result.setErrno(0);
		result.setErrmsg(message);
		result.setData(data);
		result.setSuccess(true);
		return result;
	}

	public static  <T> Result<T> ok(String errmsg, T data) {
		Result<T> result = new Result<T>();
		result.setErrno(0);
		result.setErrmsg(errmsg);
		result.setData(data);
		result.setSuccess(true);
		return result;
	}

	public static <T> Result<T> fail() {
		Result<T> result = new Result<T>();
		result.setErrno(-1);
		result.setErrmsg("错误");
		result.setSuccess(false);
		return result;
	}

	public static <T> Result<T> fail(String errmsg) {
		Result<T> result = new Result<T>();
		result.setErrno(-1);
		result.setErrmsg(errmsg);
		result.setSuccess(false);
		return result;
	}

	public static <T> Result<T> fail(int errno, String errmsg) {
		Result<T> result = new Result<T>();
		result.setErrno(errno);
		result.setErrmsg(errmsg);
		result.setSuccess(false);
		return result;
	}

	public static <T> Result<T> serverError() {
		Result<T> result = new Result<T>();
		result.setErrno(RetCodeEnum.CODE_500.getCode());
		result.setErrmsg(RetCodeEnum.CODE_500.getDesc());
		result.setSuccess(false);
		return result;
	}

	public static Result badArgument() {
		return fail(401, "参数不对");
	}

	public static Result badArgument(String message) {
		return fail(405, message);
	}

	public static Result badArgumentValue() {
		return fail(402, "参数值不对");
	}

	public static Result tokenInvalid() {
		return fail(501, "token认证失败");
	}

	public static Result serious() {
		return fail(502, "系统内部错误");
	}

	public static Result unsupport() {
		return fail(503, "业务不支持");
	}

	public static Result updatedDateExpired() {
		return fail(504, "更新数据已经失效");
	}

	public static Result updatedDataFailed() {
		return fail(505, "更新数据失败");
	}

	public static Result unauthz() {
		return fail(506, "无操作权限");
	}

	public static Result nExistUser() {
		return fail(507, "用户不存在");
	}

	public static Result nExistDpt() {
		return fail(508, "部门不存在");
	}

	public static Result noPerms() {
		return fail(509,"您没有权限调用此接口");
	}

}
