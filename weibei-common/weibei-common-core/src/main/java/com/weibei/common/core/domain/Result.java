package com.weibei.common.core.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author ZP
 */
@Data
@ApiModel
public class Result<T> {
	private boolean success = true;

	@ApiModelProperty
	private T data;
	//成功与否标识
	private int errno;
	//错误标识
	private String errmsg;

	public void setData(T data) {
		this.data = data;
	}

	public T getData() {
		return data;
	}

	@Override
	public String toString() {
		return "Result [errno=" + errno + " ,success=" + success + ", errmsg=" + errmsg +  ", data=" + data + "]";
	}


}
