package com.weibei.common.utils.jd;

/**
 * @Author: maochenfei
 * @Date: 2019/4/17
 * @Description:
 */
public class AutologinParamUtil {
    /**
     * 参数加密
     *
     * @param srcStr 要加密的原始字符串
     * @param rsaEncryptPubKey 加密RSA公钥
     * @return
     */
    public static String encryptParam(String srcStr, String rsaEncryptPubKey) {
        try {
            byte[] s1Byte = RsaUtil.encryptByPublicKey(srcStr.getBytes(),rsaEncryptPubKey);
            String s1 = RsaUtil.encryptBASE64(s1Byte);
            return s1;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 参数解密
     * @param encryptedStr 已加密的字符串
     * @param rsaSignPrivKey 加密RSA私钥
     * @return
     */
    public static String decryptParam(String encryptedStr, String rsaSignPrivKey) {
        try {
            byte[] s1EncryptedByte = RsaUtil.decryptBASE64(encryptedStr);
            byte[] s1Byte = RsaUtil.decryptByPrivateKey(s1EncryptedByte, rsaSignPrivKey);
            return new String(s1Byte);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
