package com.weibei.common.core.domain;

import okhttp3.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class HttpClient {
    private static OkHttpClient okHttpClient = null;
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    public static final MediaType XML = MediaType.parse("application/xml; charset=utf-8");
    private static final MediaType FORMDATA = MediaType.parse("application/x-www-form-urlencoded; charset=UTF-8");

    public static OkHttpClient getClient() {
        if (okHttpClient == null) {
            //加同步安全
            synchronized (HttpClient.class) {
                if (okHttpClient == null) {
                    okHttpClient = new OkHttpClient.Builder()
                            .connectTimeout(10, TimeUnit.SECONDS)
                            .writeTimeout(10, TimeUnit.SECONDS)
                            .readTimeout(30, TimeUnit.SECONDS)
                            .build();
                }
            }

        }

        return okHttpClient;
    }

    public static String getReqest(String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .build();
        OkHttpClient client = getClient();
        Call call = client.newCall(request);
        Response response = call.execute();
        return response.body().string();
    }

    public static String getReqest(Request request) throws IOException {
        OkHttpClient client = getClient();
        Call call = client.newCall(request);
        Response response = call.execute();
        return response.body().string();
    }

    public static Map<String,Object> postRequest(String url, String json) throws IOException {
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        Response response = getClient().newCall(request).execute();
        Map<String,Object> retMap = new HashMap<>();
        retMap.put("status", response.code());
        retMap.put("data", response.body().string());
        return retMap;
    }
    public static String postRequest2(String url, String json) throws IOException {
        RequestBody body = RequestBody.create(FORMDATA, json);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        Response response = getClient().newCall(request).execute();
        String result = response.body().string();
        return result;
    }

    public static String postFormRequest(String url, Map<String, Object> data) throws IOException {
        FormBody.Builder builder = new FormBody.Builder();
        for (Map.Entry<String,Object> obj :data.entrySet()) {
            builder.add(obj.getKey(), String.valueOf(obj.getValue()));
        }
        FormBody formBody = builder.build();
        Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .build();
        Response response = getClient().newCall(request).execute();
        String result = response.body().string();
        return result;
    }

    public static String postRequest3(String url, String json,String dealerId,String requestId) throws IOException {
        RequestBody body = RequestBody.create(FORMDATA, json);
        Request request = new Request.Builder()
                .url(url).addHeader("dealer-id",dealerId).addHeader("request-id",requestId)
                .post(body)
                .build();
        Response response = getClient().newCall(request).execute();
        String result = response.body().string();
        return result;
    }
    public static String postXMLRequest(String url, String xml) throws IOException {
        RequestBody body = RequestBody.create(FORMDATA, xml);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        Response response = getClient().newCall(request).execute();
        return response.body().string();
    }
    public static Response postRequestFormdata(String url, Headers headers, RequestBody body ) throws Exception{
//        RequestBody body = RequestBody.create(FORMDATA, json);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .headers(headers)
                .build();
        return getClient().newCall(request).execute();
    }

    public static Response getRequestFormdata(String url, Headers headers, String json) throws Exception{
        Request request = new Request.Builder()
                .url(url)
                .headers(headers)
                .build();
        OkHttpClient client = getClient();
        Call call = client.newCall(request);
        return call.execute();
    }

    public static Map<String,Object> postRequest5(String url,FormBody formBody) throws IOException {

//        RequestBody body = RequestBody.create(FORMDATA, json);
        Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .build();
        Response response = getClient().newCall(request).execute();
        Map<String,Object> retMap = new HashMap<>();
        retMap.put("status", response.code());
        retMap.put("data", response.body().string());
        return retMap;
    }
}
